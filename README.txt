This is my first game that is accessible to public. It's simple Arkanoid clone with few additional quirks.
Have fun! ^_^

 ===============================================
|                Translations                   |

 ===============================================
Game has two translations: polish and english.
Translations are in json files in StreamingAssets/Translations folder.
To add new translations for language "Language" create new translation file "Language.json", and add entry "Language" to index.json file in the same folder.

 ===============================================
|                Sets                           |
 ===============================================

Sets are placed in json files in StreamingAssets/Sets. Like translations you need to add both json file, and update index.json file.
All fields are required and cannot be null, because I'm using jsonutility class for serialization and deserialization. I'll try to make it simpler in future, but it's not top priority.
Json structure file:

Set:
* Name (string) - name of the set.
* Time (int) - base time bonus for each set.  Time 1 gives 10 initial bonus. Bonus is decreased by 10 each second.
* Background (string) - background file for set. Background file must exists in StreamingAssets/Background folder. Keep in mind that supported aspect ratios are 4:3 and 5:4.
* Levels (Level[]) - array of levels
* Alpha (float) - how transparent should be background. 1 is not transparent at all. I find that value a little to bright and distracting during the game, but it depends on picture used.

Level:
*  Blocks (Block[]) - array of blocks


Block:
* position (Vector2)  - position of the block. Each block has width of 5 and height of 2. Blocks should be place on coordinate x between -64 and 64, and coordinate y between 49 and -39.
* type (string) - has 3 possible values:
    - "INDEST" for indestructible block
    - "TELEPORT" for teleport block
    - "NORMAL" for every other block
* color (Color) - Color of the block. Only used on normal not-shooting blocks. Otherwise value is ignored.
* MaxHP (int) - How many hitpoints has the block.
* TeleportNo (int) - Used only for teleport blocks. Otherwise value is ignored. It is used to match teleport blocks. Only two teleport blocks should have the same number on the level.
* WallExtension (BitFlag) - Only used for indestructible walls. Otherwise is ignored. 
It is used to extend colliders of wall bricks to make box collider continuous. Otherwise ball kept going between two colliders.
First bit (1) extends wall up.
Second bit (2) extends wall left.
Third bit (4) extends wall down.
Fourth bit (8) extend wall right.
So, for example:
Right down corner extends to up and left, so value is 1 + 2 = 3
Middle of the wall extends everywhere, so value is 1 + 2 + 4 + 8 = 15
Middle of vertical wall extends left and right, so value is 2 + 8 = 10
Single block should not extends, so value is 0
* MovingParams (MovingParam) - parameters for movement of the block.
* ShootingParams (ShootingParam) - parameters for how the block shoots.


MovingParam:
* Movement (Vector2[]) - pattern of block movements. If initial position of the block is a, and Movement is [b,c] then pattern of movement is a->b->c->a->b->c->...
* Speed (float) - speed of the movement
* NotNull (bool) - should be true if block is actually moving. Otherwise moving parameters are ignored.

ShootingParams:
* Interval (int) - shooting interval in seconds.
* Offset (int) - waiting time before first shot.
* BulletSpeed (float) - speed of the bullet.
* IntervalRandomness (Pair(int)) - actual interval is interval + random value between this pair.
* OffsetRandomness (Pair(int)) - actual offset is offset + random value between this pair.
* NotNull (bool) - should be true if block is actually shooting. Otherwise shooting parameters are ignored.

 ===============================================
|                Copyrights                     |
 ===============================================
 
Source code is available at https://bitbucket.org/anomaly2/arkanoid

All functions that are copied from sites like stackoverflow have url in comments and are under sites' respective licenses.

Other source code is under MIT license.

All sounds and music are downloaded from https://freesound.org and are under CC0 License (https://creativecommons.org/publicdomain/zero/1.0/)

Brick wall texture is from http://www.highresolutiontextures.com/stone-wall-brick-wall-free-textures

All other assets (including backgrounds) are made by me and are under CC0 License (https://creativecommons.org/publicdomain/zero/1.0/)