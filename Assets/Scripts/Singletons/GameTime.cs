﻿//Class responsible for updating time variable during the game.

using JetBrains.Annotations;

public class GameTime : WeakSingleton<GameTime> {
    protected GameTime() {
    }

    public int TotalTime {
        get {
            return SetTracker.Instance.TotalTime;
        }
        set {
            SetTracker.Instance.TotalTime = value;
        }
    }

    protected void Start () {
        InvokeRepeating("UpdateTime", 0, 1);
    }

    [UsedImplicitly]
    private void UpdateTime() {
        if (!GamePlay.Instance.IsPaused) {
            TotalTime++;
        }
    }
}
