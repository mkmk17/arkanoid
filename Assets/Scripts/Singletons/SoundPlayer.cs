﻿using UnityEngine;

//Class responsible for managing sound effects and music.
public class SoundPlayer : Singleton<SoundPlayer> {
    protected SoundPlayer() {
    }

    AudioSource BlockSound, PaddleSound, BonusSound, LifeLoss, Music, Teleport;

    void InitSound(ref AudioSource source, string path, bool music = false) {
        source = gameObject.AddComponent<AudioSource>();
        source.clip = Resources.Load(path) as AudioClip;
        source.playOnAwake = false;
        source.loop = music;
    }

	void Awake () {
        InitSound(ref BlockSound, @"Sounds\block");
        InitSound(ref PaddleSound, @"Sounds\paddle");
        InitSound(ref BonusSound, @"Sounds\bonus");
        InitSound(ref LifeLoss, @"Sounds\life_loss");
        InitSound(ref Teleport, @"Sounds\teleport");
        InitSound(ref Music, @"Sounds\music", true);
    }

    void PlaySound(AudioSource source) {
        if (PlayerPrefs.GetInt("Sound", 1) == 0 || source.isPlaying) {
            return;
        }
        source.Play();
    }

    float SoundLength(AudioSource source) {
        if (PlayerPrefs.GetInt("Sound", 1) == 0) {
            return 0;
        }
        return source.clip.length;
    }

    public void PlayBlockSound() {
        PlaySound(BlockSound);
    }

    public float BlockSoundLength() {
        return SoundLength(BlockSound);
    }

    public void PlayBonusSound() {
        PlaySound(BonusSound);
    }

    public float BonusSoundLength() {
        return SoundLength(BonusSound);
    }

    public void PlayPaddleSound() {
        PlaySound(PaddleSound);
    }

    public float PaddleSoundLength() {
        return SoundLength(PaddleSound);
    }

    public void PlayLifeLoss() {
        PlaySound(LifeLoss);
    }

    public float LifeLossLength() {
        return SoundLength(LifeLoss);
    }

    public void PlayTeleportSound() {
        PlaySound(Teleport);
    }

    public float TeleportSoundLength() {
        return SoundLength(Teleport);
    }

    public void StartMusic() {
        if (PlayerPrefs.GetInt("Music", 1) == 0) {
            return;
        }
        Music.Play();
    }

    public void PauseMusic() {
        if (PlayerPrefs.GetInt("Music", 1) == 0) {
            return;
        }
        Music.Pause();
    }

    public void UnPauseMusic() {
        if (PlayerPrefs.GetInt("Music", 1) == 0) {
            return;
        }
        Music.UnPause();
    }

    public void StopMusic() {
        if (PlayerPrefs.GetInt("Music", 1) == 0) {
            return;
        }
        Music.Stop();
    }

}
