﻿using System;
using UnityEngine;

[Flags]
public enum WallExtension {
    None = 0,
    Up = 1,
    Left = 2,
    Down = 4,
    Right = 8
}

//Class responsible level creation.
public class LevelCreator : Singleton<LevelCreator> {
    protected LevelCreator() {
    }

    public static bool HasFlag(WallExtension val, WallExtension flag) {
        return (val & flag) == flag;
    }


    private GameObject simpleBlockPrefab;
    private GameObject impenetrableBlockPrefab;
    private GameObject teleportBlockPrefab;
    private GameObject movingSimpleBlockPrefab;
    private GameObject movingImpenetrableBlockPrefab;
    private GameObject movingTeleportBlockPrefab;
    private GameObject stationaryEnemy;
    private GameObject movingEnemy;

    private void Awake() {
        simpleBlockPrefab = (GameObject)Resources.Load(@"Prefabs\Blocks\Block", typeof(GameObject));
        impenetrableBlockPrefab = (GameObject)Resources.Load(@"Prefabs\Blocks\ImpenetrableBlock", typeof(GameObject));
        teleportBlockPrefab = (GameObject)Resources.Load(@"Prefabs\Blocks\TeleportBlock", typeof(GameObject));
        movingSimpleBlockPrefab = (GameObject)Resources.Load(@"Prefabs\Blocks\MovingBlock", typeof(GameObject));
        movingImpenetrableBlockPrefab = (GameObject)Resources.Load(@"Prefabs\Blocks\MovingImpenetrableBlock", typeof(GameObject));
        movingTeleportBlockPrefab = (GameObject)Resources.Load(@"Prefabs\Blocks\MovingTeleportBlock", typeof(GameObject));
        stationaryEnemy = (GameObject)Resources.Load(@"Prefabs\Blocks\StationaryEnemy", typeof(GameObject));
        movingEnemy = (GameObject)Resources.Load(@"Prefabs\Blocks\MovingEnemy", typeof(GameObject));
    }

    private Transform GetBlockParent() {
        return GameObject.FindGameObjectWithTag("BlockSpawn").transform;
    }

    public bool IsLastLevel() {
        LevelInfo level = SetTracker.Instance.CurrentLevel;
        return
            level.LevelNo == 10;
    }

    private bool MovingParamsIsNull(FMovingParams movingParams) {
        if (movingParams == null)
            return true;
        return !movingParams.NotNull;
    }

    private bool ShootingParamsIsNull(ShootingIntervalParams shootingParams) {
        if (shootingParams == null)
            return true;
        return !shootingParams.NotNull;
    }

    private void CreateLevel(FLevel level)
    {
        for (int i = 0; i < level.Blocks.Count; i++)
        {
            FBlock block = level.Blocks[i];
            if (block == null) continue;
            switch (block.Type)
            {
                case "NORMAL":
                    CreateNormalBlock(block);
                    break;
                case "INDEST":
                    CreateIndestructibleBlock(block);
                    break;
                case "TELEPORT":                   
                    int partnerIndex = level.Blocks.FindLastIndex((b) => b.Type == "TELEPORT" && b.TeleportNo == block.TeleportNo);
                    CreateTeleportBlocks(block, level.Blocks[partnerIndex]);
                    level.Blocks[i] = null;
                    level.Blocks[partnerIndex] = null;
                    break;
                default:
                    Debug.LogError("Uknown block type: "  + block.Type);
                    break;
            }
        }
    }

    private void CreateTeleportBlocks(FBlock block1, FBlock block2)
    {
        GameObject prefab1 = MovingParamsIsNull(block1.MovingParams) ? teleportBlockPrefab : movingTeleportBlockPrefab;
        GameObject prefab2 = MovingParamsIsNull(block2.MovingParams) ? teleportBlockPrefab : movingTeleportBlockPrefab;
        var teleport1 = Instantiate(prefab1, new Vector3(block1.Position.x, block1.Position.y, 0), Quaternion.identity, GetBlockParent());
        var teleport2 = Instantiate(prefab2, new Vector3(block2.Position.x, block2.Position.y, 0), Quaternion.identity, GetBlockParent());
        teleport1.GetComponent<TeleportBlock>().LinkedTeleport = teleport2;
        teleport2.GetComponent<TeleportBlock>().LinkedTeleport = teleport1;
        if (!MovingParamsIsNull(block1.MovingParams)) {
            teleport1.transform.GetComponent<PatternMovingBlock>().speed = block1.MovingParams.Speed != -1 ? block1.MovingParams.Speed : UnityEngine.Random.Range(5, 25);
            teleport1.transform.GetComponent<PatternMovingBlock>().pattern = block1.MovingParams.Movement;
        }
        if (!MovingParamsIsNull(block2.MovingParams)) {
            teleport2.transform.GetComponent<PatternMovingBlock>().speed = block2.MovingParams.Speed != -1 ? block2.MovingParams.Speed: UnityEngine.Random.Range(5, 25);
            teleport2.transform.GetComponent<PatternMovingBlock>().pattern = block2.MovingParams.Movement;
        }
        BlockManager.Instance.CreateBlock(teleport1.GetComponent<Block>());
        BlockManager.Instance.CreateBlock(teleport2.GetComponent<Block>());
    }

    private void CreateIndestructibleBlock(FBlock block)
    {
        GameObject prefab = MovingParamsIsNull(block.MovingParams) ? impenetrableBlockPrefab : movingImpenetrableBlockPrefab;
        GameObject go = Instantiate(prefab, new Vector3(block.Position.x, block.Position.y, 0), Quaternion.identity, GetBlockParent());
        AddWallExtensions(go.GetComponent<BoxCollider2D>(), block.WallExtension);
        if (!MovingParamsIsNull(block.MovingParams))
        {
            go.transform.GetComponent<PatternMovingBlock>().speed = block.MovingParams.Speed != -1
                ? block.MovingParams.Speed
                : UnityEngine.Random.Range(5, 25);
            go.transform.GetComponent<PatternMovingBlock>().pattern = block.MovingParams.Movement;
        }
        BlockManager.Instance.CreateBlock(go.transform.GetComponent<Block>());
    }

    private void CreateNormalBlock(FBlock block)
    {
        GameObject prefab;
        if (ShootingParamsIsNull(block.ShootingParams))
        {
            prefab = MovingParamsIsNull(block.MovingParams) ? simpleBlockPrefab : movingSimpleBlockPrefab;
        }
        else
        {
            prefab = MovingParamsIsNull(block.MovingParams) ? stationaryEnemy : movingEnemy;
        }
        GameObject go = Instantiate(prefab, new Vector3(block.Position.x, block.Position.y, 0), Quaternion.identity, GetBlockParent());
        go.transform.GetComponent<SimpleBlock>().BlockColor = ShootingParamsIsNull(block.ShootingParams) ? block.Color : Color.white;
        go.transform.GetComponent<SimpleBlock>().MaxHP = block.MaxHp;
        if (!ShootingParamsIsNull(block.ShootingParams))
        {
            go.transform.GetComponent<IntervalShootingBlock>().Init(block.ShootingParams);
        }
        if (!MovingParamsIsNull(block.MovingParams))
        {
            go.transform.GetComponent<PatternMovingBlock>().speed = block.MovingParams.Speed;
            go.transform.GetComponent<PatternMovingBlock>().pattern = block.MovingParams.Movement;
        }
        BlockManager.Instance.CreateBlock(go.transform.GetComponent<Block>());
    }


    public void CreateLevel()
    {
        FSet currentSet = SetTracker.Instance.LoadedSet;
        SetTracker.Instance.BaseTime = currentSet.Time;
        GameObject.FindWithTag("Background").GetComponent<SpriteRenderer>().sprite = SetTracker.Instance.LoadedSetBackground;
        GameObject.FindWithTag("Background").GetComponent<SpriteRenderer>().color = new Color(1,1,1, SetTracker.Instance.LoadedSetBackgroundAlpha);
        GameObject.FindWithTag("Background").GetComponent<SpriteResizer>().ResizeSprite();
        LevelInfo level = SetTracker.Instance.CurrentLevel;
        CreateLevel(currentSet.Levels[level.LevelNo-1]);
    }


    private void AddWallExtensions(BoxCollider2D boxCollider, WallExtension wall) {
        const float HORIZONTAL_EXTENSION = 0.5f;
        const float VERTICAL_EXTENSION = 0.5f; //boxCollider will go more far vertically by 2*verticalextension
        // Example if we want boxCollider to go 1 unit down then offset -= 0.5u and size.y += u
        if (HasFlag(wall, WallExtension.Up)) {
            boxCollider.offset = new Vector2(boxCollider.offset.x, boxCollider.offset.y + VERTICAL_EXTENSION);
            boxCollider.size = new Vector2(boxCollider.size.x, boxCollider.size.y + 2 * VERTICAL_EXTENSION);
        }
        if (HasFlag(wall, WallExtension.Down)) {
            boxCollider.offset = new Vector2(boxCollider.offset.x, boxCollider.offset.y - VERTICAL_EXTENSION);
            boxCollider.size = new Vector2(boxCollider.size.x, boxCollider.size.y + 2 * VERTICAL_EXTENSION);
        }
        if (HasFlag(wall, WallExtension.Left)) {
            boxCollider.offset = new Vector2(boxCollider.offset.x - HORIZONTAL_EXTENSION, boxCollider.offset.y);
            boxCollider.size = new Vector2(boxCollider.size.x + 2 * HORIZONTAL_EXTENSION, boxCollider.size.y);
        }
        if (HasFlag(wall, WallExtension.Right)) {
            boxCollider.offset = new Vector2(boxCollider.offset.x + HORIZONTAL_EXTENSION, boxCollider.offset.y);
            boxCollider.size = new Vector2(boxCollider.size.x + 2 * HORIZONTAL_EXTENSION, boxCollider.size.y);
        }
    }
}