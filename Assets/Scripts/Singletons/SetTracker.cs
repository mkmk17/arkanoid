﻿//Data structure to handle keeping score and life between levels in set.

using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SetTracker : Singleton<SetTracker> {
    protected SetTracker() {
        Score = 0;
        Life = 3;
        TotalTime = 0;
        CurrentLevel = new LevelInfo("A");
    }

    private const int SCORE_MULTIPLIER = 10;
    public int BaseTime = 999;

    public List<string> SetList = new List<string>();
    //Levels can be pretty big and many of them. Let's load just one to memory.
    public FSet LoadedSet;

    public Sprite LoadedSetBackground;
    public float LoadedSetBackgroundAlpha;
    public bool Ready = false;

    public int TimeLeft {
        get {
            return BaseTime - TotalTime;
        }
    }

    public int TotalTime {
        get; set;
    }

    public int Score {
        get; set;
    }

    public int Life {
        get; set;
    }

    public LevelInfo CurrentLevel {
        get; set;
    }

    public int TimeBonus {
        get {
            return TimeLeft * SCORE_MULTIPLIER;
        }
    }

    public int RealScore {
        get {
            return Score + TimeBonus;
        }
    }

    private void LoadSetBackground(string backgroundName)
    {
        StartCoroutine(FileHandler.GetFileHandler(Path.Combine(Application.streamingAssetsPath, "Backgrounds"))
            .GetSprite(backgroundName,
                (sprite) =>
                {
                    LoadedSetBackground = sprite;
                    Ready = true;
                }));
    }

    public void GetSetDefinition(string setName)
    {
        Ready = false;
        if (LoadedSet != null && LoadedSet.Name == setName)
        {
            LoadSetBackground(LoadedSet.Background);
            return;
        }
        StartCoroutine(FileHandler.GetFileHandler(Path.Combine(Application.streamingAssetsPath, "Sets"))
            .GetFile(setName + ".json", 
            (setString) =>
            {
                LoadedSet = JsonUtility.FromJson<FSet>(setString);
                LoadedSetBackgroundAlpha = LoadedSet.Alpha;
                LoadSetBackground(LoadedSet.Background);
            }));
    }

    public void Init() {
        SetList.Clear();
        StartCoroutine(FileHandler.GetFileHandler(Path.Combine(Application.streamingAssetsPath, "Sets"))
            .GetFile("index.json",
                (indexString) => {
                    IndexData indexData = JsonUtility.FromJson<IndexData>(indexString);
                    foreach (var set in indexData.IndexEntries)
                    {
                       SetList.Add(set.IndexEntry);
                    }
                    Ready = true;
                }));
    }
}
