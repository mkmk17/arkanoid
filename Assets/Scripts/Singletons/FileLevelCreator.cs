﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.IO;

[Serializable]
public class FLevel
{
    public List<FBlock> Blocks;
}

[Serializable]
public class FBlock
{
    public Vector2 Position;
    public string Type; // "NORMAL" / "INDEST" / "TELEPORT"
    public Color Color;
    public FMovingParams MovingParams;
    public ShootingIntervalParams ShootingParams;
    public int MaxHp;
    public WallExtension WallExtension;
    public int TeleportNo;
}

[Serializable]
public class FMovingParams {
    public List<Vector2> Movement;
    public float Speed;
    public bool NotNull; //JSONUtility saves nulls as empty class.
}

[Serializable]
public class FSet
{
    public string Name;
    public string Background;
    public List<FLevel> Levels;
    public int Time;
    public float Alpha;
}

public class BlockCoordinates {
    public float X;
    public float Y;
    public int XIndex;
    public int YIndex;

    public BlockCoordinates(float x, float y, int xIndex, int yIndex) {
        X = x;
        Y = y;
        XIndex = xIndex;
        YIndex = yIndex;
    }
}


/*Example usage
 * 
 * 	    FileLevelCreator.Instance.WriteSet("A","A.png", 999);
	    FileLevelCreator.Instance.WriteSet("B", "B.png", 999);
	    FileLevelCreator.Instance.WriteSet("C", "C.png", 999);
	    FileLevelCreator.Instance.WriteSet("D", "D.png", 999);
	    FileLevelCreator.Instance.WriteSet("E", "E.png", 999);
        */

//Class responsible level creation.
public class FileLevelCreator : Singleton<FileLevelCreator>
{

    private string outputPath;

    protected FileLevelCreator()
    {
        teleportPairNo = 0;       
    }

    protected void Awake() {
        outputPath = Path.Combine(Application.streamingAssetsPath, "Sets");
    }

    private int teleportPairNo;

    public void WriteSet(string setName, string backGround, int time) {
        using (StreamWriter writer = new StreamWriter(Path.Combine(outputPath, setName + ".json")))
        {
            FSet set = new FSet {Levels = new List<FLevel>(), Name = setName, Background = backGround, Time = time};
            for (int i = 1; i <= 10; i++)
            {
                FLevel level;
                switch (setName)
                {
                    case "A":
                        level = SetA(i);
                        break;
                    case "B":
                        level = SetB(i);
                        break;
                    case "C":
                        level = SetC(i);
                        break;
                    case "D":
                        level = SetD(i);
                        break;
                    case "E":
                        level = SetE(i);
                        break;
                    default:
                        throw new Exception("Unknown set");
                }
                set.Levels.Add(level);
            }
            string levelString = JsonUtility.ToJson(set, true);
            writer.Write(levelString);
        }

    }


    public static bool HasFlag(WallExtension val, WallExtension flag) {
        return (val & flag) == flag;
    }

    private List<Color> BlockColors(bool multiHp) {
        List<Color> ret = new List<Color>{
                        Color.blue,
                        Color.cyan,
                        Color.green,
                        Color.white,
                        Color.red,
         };
        if (multiHp) {
            return ret.Select(color => Color.Lerp(color, Color.black, 0.2f)).ToList();
        }
        return ret;
    }

    private const float TIGHT_Y_PADDING = 2;
    private const float LOOSE_Y_PADDING = 3;

    private const float TIGHT_X_PADDING = 5.25f;
    private const float LOOSE_X_PADDING = 6;
    private const float VERY_LOOSE_X_PADDING = 7;




    #region BLOCK_CREATION

    Color ColorBlockByRow(int maxHp, int ix, int iy) {
        List<Color> blockColors = BlockColors(maxHp > 1);
        return blockColors[iy % blockColors.Count];
    }

    Color ColorBlockByColumn(int maxHp, int ix, int iy) {
        List<Color> blockColors = BlockColors(maxHp > 1);
        return blockColors[ix % blockColors.Count];
    }

    Color ColorBlockRandom(int maxHp, int ix, int iy) {
        List<Color> blockColors = BlockColors(maxHp > 1);
        return blockColors[UnityEngine.Random.Range(0, blockColors.Count-1)];
    }

    private List<FBlock> CreateBlocks(Vector2 start, Pair<int> size, Func<BlockCoordinates, FBlock> defaultBlock,
        Pair<float> padding, Func<int, int, bool> useDefault = null, Func<BlockCoordinates, FBlock> otherBlocksAction = null) {
        List<FBlock> blocks = new List<FBlock>();
        if (useDefault == null) {
            useDefault = (x, y) => true;
        }
        if (otherBlocksAction == null) {
            otherBlocksAction = (c) => null;
        }
        int j = 0;
        for (float x = start.x; j < size.First; x += padding.First) {
            int i = 0;
            for (float y = start.y; i < size.Second; y += padding.Second) {
                if (useDefault(j, i)) {
                    FBlock block = defaultBlock(new BlockCoordinates(x, y, j, i));
                    if (block != null) {
                        blocks.Add(block);
                    }
                }
                else {
                    FBlock block = otherBlocksAction(new BlockCoordinates(x, y, j, i));
                    if (block != null) {
                        blocks.Add(block);
                    }
                }
                i++;
            }
            j++;
        }
        return blocks;
    }

    private Func<BlockCoordinates, FBlock> CreateSimpleBlock(int maxHp = 1, Func<int, int ,int , Color> coloring = null) {
        if (coloring == null) {
            coloring = ColorBlockByRow;
        }
        return (c) =>
        {
            FBlock block = new FBlock
            {
                Type = "NORMAL",
                MaxHp = maxHp,
                Color = coloring(maxHp, c.XIndex, c.YIndex),
                MovingParams = null,
                Position = new Vector2(c.X, c.Y),
                ShootingParams = null
            };
            return block;
        };
    }

    private Func<BlockCoordinates, FBlock> CreateSimpleMovingBlock(float? speed, Func<float, float, List<Vector2>> pattern, int maxHp = 1, Func<int, int, int, Color> coloring = null) {
        if (coloring == null) {
            coloring = ColorBlockByRow;
        }
        return (c) =>
        {
            FMovingParams movingParams = new FMovingParams()
            {
                Movement = pattern(c.X, c.Y),
                Speed = speed ?? UnityEngine.Random.Range(5, 25),
                NotNull = true
                
            };
            FBlock block = new FBlock {
                Type = "NORMAL",
                MaxHp = maxHp,
                Color = coloring(maxHp, c.XIndex, c.YIndex),
                MovingParams = movingParams,
                Position = new Vector2(c.X, c.Y),
                ShootingParams = null
            };
            return block;
        };
    }

    private Func<BlockCoordinates, FBlock> CreateImpenetrableBlock(Func<BlockCoordinates, WallExtension> wallFunc = null) {
        if (wallFunc == null) {
            wallFunc = (c) => WallExtension.None;
        }
        return (c) => {
            FBlock block = new FBlock {
                Type = "INDEST",
                Position = new Vector2(c.X, c.Y),
                WallExtension = wallFunc(c),
            };
            return block;
        };
    }

    private Func<BlockCoordinates, FBlock> CreateImpenetrableMovingBlock(float? speed, Func<float, float, List<Vector2>> pattern, Func<BlockCoordinates, WallExtension> wallFunc = null) {
        if (wallFunc == null) {
            wallFunc = (c) => WallExtension.None;
        }
        return (c) =>
        {
            FMovingParams movingParams = new FMovingParams()
            {
                Movement = pattern(c.X, c.Y),
                Speed = speed ?? UnityEngine.Random.Range(5, 25),
                NotNull = true
            };
            FBlock block = new FBlock {
                Type = "INDEST",
                Position = new Vector2(c.X, c.Y),
                WallExtension = wallFunc(c),
                MovingParams = movingParams
            };
            return block;
        };
    }

    private Func<BlockCoordinates, FBlock> CreateShootingBlock(Func<BlockCoordinates, ShootingIntervalParams> shootingFunc = null, int maxHp = 1) {
        if (shootingFunc == null) {
            shootingFunc = (c) => new ShootingIntervalParams();
        }
        return (c) => {
            FBlock block = new FBlock {
                Type = "NORMAL",
                MaxHp = maxHp,
                MovingParams = null,
                Position = new Vector2(c.X, c.Y),
                ShootingParams = shootingFunc(c)
            };
            return block;
        };
    }

    private Func<BlockCoordinates, FBlock> CreateMovingShootingBlock(float? speed, Func<float, float, List<Vector2>> pattern, Func<BlockCoordinates, ShootingIntervalParams> shootingFunc = null, int maxHp = 1) {
        if (shootingFunc == null) {
            shootingFunc = (c) => new ShootingIntervalParams();
        }
        return (c) => {
            FMovingParams movingParams = new FMovingParams() {
                Movement = pattern(c.X, c.Y),
                Speed = speed ?? UnityEngine.Random.Range(5, 25),
                NotNull = true,
            };
            FBlock block = new FBlock {
                Type = "NORMAL",
                MaxHp = maxHp,
                MovingParams = movingParams,
                Position = new Vector2(c.X, c.Y),
                ShootingParams = shootingFunc(c)
            };
            return block;
        };
    }

    List<FBlock> CreateTeleportBlocks(Vector2 pos1, Vector2 pos2, float? speed1 = null, List<Vector2> pattern1 = null, float? speed2 = null, List<Vector2> pattern2 = null)
    {
        List<FBlock> blocks = new List<FBlock>();
        FMovingParams movingParams1 = new FMovingParams() {
            Movement = pattern1,
            Speed = speed1 ?? UnityEngine.Random.Range(5, 25),
            NotNull = pattern1 != null
        };
        FBlock block1 = new FBlock {
            Type = "TELEPORT",
            MovingParams = movingParams1,
            Position = pos1,
            TeleportNo = teleportPairNo,
        };
        blocks.Add(block1);
        FMovingParams movingParams2 = new FMovingParams() {
            Movement = pattern2,
            Speed = speed2 ?? UnityEngine.Random.Range(5, 25),
            NotNull = pattern2 != null
        };
        FBlock block2 = new FBlock {
            Type = "TELEPORT",
            MovingParams = movingParams2,
            Position = pos2,
            TeleportNo = teleportPairNo,
        };
        teleportPairNo++;
        blocks.Add(block2);
        return blocks;
    }

    #endregion BLOCK_CREATION

   private  FLevel SetA(int levelNo) {
        switch (levelNo) {
            case 1: {
                return new FLevel()
                {
                    Blocks = CreateBlocks(new Vector2(-36, 20), new Pair<int>(13, 5), CreateSimpleBlock(),
                        new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING))
                };
            }
            case 2: {
                return new FLevel() { 
                Blocks = CreateBlocks(new Vector2(-33, 0), new Pair<int>(12, 12), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                        (ix, iy) => iy <= ix)};
                }
            case 3: {
                FLevel level = new FLevel() {Blocks = new List<FBlock>()};
                level.Blocks.AddRange(CreateBlocks(new Vector2(-50, 20), new Pair<int>(5, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                level.Blocks.AddRange(CreateBlocks(new Vector2(26, 20), new Pair<int>(5, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                return level;
            }
            case 4: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                level.Blocks.AddRange(CreateBlocks(new Vector2(-50, 40), new Pair<int>(4, 3), CreateSimpleBlock(), new Pair<float>(TIGHT_X_PADDING, TIGHT_Y_PADDING)));
                level.Blocks.AddRange(CreateBlocks(new Vector2(-50, -20), new Pair<int>(4, 3), CreateSimpleBlock(), new Pair<float>(TIGHT_X_PADDING, TIGHT_Y_PADDING)));
                level.Blocks.AddRange(CreateBlocks(new Vector2(35, -20), new Pair<int>(4, 3), CreateSimpleBlock(), new Pair<float>(TIGHT_X_PADDING, TIGHT_Y_PADDING)));
                level.Blocks.AddRange(CreateBlocks(new Vector2(35, 40), new Pair<int>(4, 3), CreateSimpleBlock(), new Pair<float>(TIGHT_X_PADDING, TIGHT_Y_PADDING)));
                return level;
            }
            case 5: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 20), new Pair<int>(7, 9), CreateSimpleBlock(), new Pair<float>(TIGHT_X_PADDING, TIGHT_Y_PADDING),
                    (ix, iy) => iy == 0 || ix == 0 || iy == 8 || ix == 6,
                    (c) =>
                    {
                        if (c.XIndex >= 2 && c.XIndex <= 4 && c.YIndex >= 3 && c.YIndex <= 5)
                            return CreateSimpleBlock(2)(c);
                        return null;
                    }))
                ;
                level.Blocks.AddRange(CreateBlocks(new Vector2(29, 20), new Pair<int>(7, 9), CreateSimpleBlock(), new Pair<float>(TIGHT_X_PADDING, TIGHT_Y_PADDING),
                    (ix, iy) => iy == 0 || ix == 0 || iy == 8 || ix == 6,
                                            (c) => {
                                                if (c.XIndex >= 2 && c.XIndex <= 4 && c.YIndex >= 3 && c.YIndex <= 5)
                                                    return CreateSimpleBlock(2)(c);
                                                return null;
                                            }));
                return level;
            }
            case 6: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-42, 30), new Pair<int>(7, 7), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                        (ix, iy) => iy >= ix));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-0, 30), new Pair<int>(7, 7), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                        (ix, iy) => (6-iy) <= ix));
                    return level;
                }
            case 7: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 48), new Pair<int>(21, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 30), new Pair<int>(21, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 12), new Pair<int>(21, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, -6), new Pair<int>(21, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            case 8: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-24, 10), new Pair<int>(9, 9), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                        (ix, iy) => !(iy >= 3 && iy <= 5 && ix >= 3 && ix <= 5), CreateSimpleBlock(2)));
                    return level;
                }
            case 9: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                level.Blocks.AddRange(CreateBlocks(new Vector2(-24, 10), new Pair<int>(9, 9), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                        (ix, iy) => !(iy == 8 || iy == 0 || ix == 0 || ix == 8), CreateSimpleBlock(2)));
                    return level;
                }
            case 10: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-30, 15), new Pair<int>(6, 5), CreateSimpleBlock(2), new Pair<float>(12, 8)));
                    return level;
                }
            default:
                throw new Exception("Level " + levelNo + " of set A not impmemented");
        }
    }

    private FLevel SetB(int levelNo) {
        switch (levelNo) {
            case 1: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 20), new Pair<int>(13, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(-36, 11), new Vector2(-36, 40)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(36, 11), new Vector2(36, 40)));
                    return level;
                }
            case 2: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-50, 20), new Pair<int>(5, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(20, 20), new Pair<int>(5, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(-38, 11), new Vector2(32, 40)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(32, 11), new Vector2(-38, 40)));
                    return level;
                }
            case 3: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-30, 48), new Pair<int>(16, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-30, 36), new Pair<int>(16, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 6), new Pair<int>(16, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, -6), new Pair<int>(16, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(-54, 42), new Vector2(54, 0)));
                    return level;
                }
            case 4: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-50, 20), new Pair<int>(5, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(26, 20), new Pair<int>(5, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(-38, 40), new Vector2(32, 40)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-2.5f, -30), new Pair<int>(1, 26), CreateSimpleBlock(2), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            case 5: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, -20), new Pair<int>(11, 9), CreateSimpleBlock(), new Pair<float>(12, 8)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(-54, -20), new Vector2(54, -20), 
                        15, 
                        new List<Vector2> {
                            new Vector2(-54,-20),
                            new Vector2(-54,44),
                        }, 
                        15,
                        new List<Vector2> {
                            new Vector2(54,44),
                            new Vector2(54,-20),
                        }
                    ));
                    return level;
                }
            case 6: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 20), new Pair<int>(12, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(-60, 40), new Vector2(0, 11),
                        15,
                        new List<Vector2> {
                            new Vector2(-60,40),
                            new Vector2(60,40),
                        }));
                    return level;
                }
            case 7: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-32.5f, 20), new Pair<int>(12, 12), CreateSimpleBlock(), new Pair<float>(TIGHT_X_PADDING, TIGHT_Y_PADDING),
                        (ix, iy) => !(ix >= 4 && ix <= 7 && iy >= 4 && iy <= 7)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(-60, 40), new Vector2(0, 34),
    25,
    new List<Vector2> {
                            new Vector2(-40,15),
                            new Vector2(-40,45),
                            new Vector2(35,45),
                            new Vector2(35,15),
    }));
                    return level;
                }
            case 8: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 40), new Pair<int>(21, 1), CreateSimpleBlock(2, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 30), new Pair<int>(21, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 20), new Pair<int>(21, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 10), new Pair<int>(21, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 0), new Pair<int>(21, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(-60, 40), new Vector2(0, 11),
                    25,
                    new List<Vector2> {
                        new Vector2(-60,45),
                        new Vector2(60,45),
                    },
                    15,
                    new List<Vector2> {
                        new Vector2(-60,-10),
                        new Vector2(60,-10),
                    }
                    ));
                    return level;
                }
            case 9: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 40), new Pair<int>(21, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 30), new Pair<int>(21, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 10), new Pair<int>(21, 1), CreateSimpleBlock(2, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 0), new Pair<int>(21, 1), CreateSimpleBlock(2, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, -10), new Pair<int>(21, 1), CreateSimpleBlock(2, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(-60, 40), new Vector2(0, 11),
                    25,
                    new List<Vector2> {
                        new Vector2(-60,20),
                        new Vector2(60,20),
                    },
                    15,
                    new List<Vector2> {
                        new Vector2(-60,-20),
                        new Vector2(60,-20),
                    }
                    ));
                    return level;
                }
            case 10: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-54, 40), new Pair<int>(19, 1), CreateSimpleBlock(2, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-54, 30), new Pair<int>(19, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-54, 20), new Pair<int>(19, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-54, 10), new Pair<int>(19, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-54, 0), new Pair<int>(19, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-54, -10), new Pair<int>(19, 1), CreateSimpleBlock(2, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(-60, 40), new Vector2(0, 11),
                    25,
                    new List<Vector2> {
                        new Vector2(-60,45),
                        new Vector2(-60,-20),
                    },
                    25,
                    new List<Vector2> {
                        new Vector2(60,-20),
                        new Vector2(60,45),
                    }
                    ));
                    return level;
                }
            default:
                throw new Exception("Level " + levelNo + " of set B not impmemented");
        }
    }

    private FLevel SetC(int levelNo) {
        switch (levelNo) {
            case 1: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-33, 20), new Pair<int>(11, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                        (ix, iy) => !(ix == 5 && iy == 2),
                        CreateImpenetrableBlock()
                        ));
                    return level;
                }
            case 2: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-33, 20), new Pair<int>(11, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                        (ix, iy) => !((ix == 0 || ix == 10) && (iy == 0 || iy == 4)),
                        CreateImpenetrableBlock()
                        ));
                    return level;
                }
            case 3: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 17), new Pair<int>(13, 7), CreateImpenetrableBlock(
                        (c) => {
                            WallExtension wall = WallExtension.None;
                            if (c.YIndex == 3 && c.XIndex > 0) {
                                wall |= WallExtension.Left;
                            }
                            if (c.YIndex == 3 && c.XIndex < 12) {
                                wall |= WallExtension.Right;
                            }
                            if (c.XIndex == 6 && (c.YIndex > 0)) {
                                wall |= WallExtension.Down;
                            }
                            if (c.XIndex == 6 && (c.YIndex < 6)) {
                                wall |= WallExtension.Up;
                            }
                            return wall;
                        }
                        ), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                           (ix, iy) =>
                                (iy == 3 || ix == 6)
                                ,
                        CreateSimpleBlock()));
                    return level;
                }
            case 4: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 20), new Pair<int>(12, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    for (int sy = 10, i = 1; sy >= -15; sy -= 5, i += 1) {
                        level.Blocks.AddRange(CreateBlocks(new Vector2(-60, sy), new Pair<int>(1, 1), CreateImpenetrableMovingBlock(6*i, (x, y) => new List<Vector2> {
                            new Vector2(x,y),
                            new Vector2(-x,y),
                        }), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    }
                    return level;
                }
            case 5: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 17), new Pair<int>(13, 7), CreateImpenetrableBlock(
                       (c) => {
                           WallExtension wall = WallExtension.None;
                           if (c.YIndex == 3 && c.XIndex > 0) {
                               wall |= WallExtension.Left;
                           }
                           if (c.YIndex == 3 && c.XIndex < 12) {
                               wall |= WallExtension.Right;
                           }
                           if (c.XIndex == 6 && (c.YIndex > 0)) {
                               wall |= WallExtension.Down;
                           }
                           if (c.XIndex == 6 && (c.YIndex < 6)) {
                               wall |= WallExtension.Up;
                           }
                           return wall;
                       }
                       ), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                          (ix, iy) =>
                               (iy == 3 || ix == 6)
                               ,
                       CreateSimpleBlock()));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(-36, 10), new Vector2(-36, 40)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(36, 10), new Vector2(36, 40)));
                    return level;
                }
            case 6: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 10), new Pair<int>(13, 9), CreateImpenetrableBlock(
                        (c) => {
                            WallExtension wall = WallExtension.None;
                            if (c.XIndex > 0) {
                                wall |= WallExtension.Left;
                            }
                            if (c.XIndex < 12) {
                                wall |= WallExtension.Right;
                            }
                            return wall;
                        }
                        ), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                            (ix, iy) => (iy == 0 || iy == 8),
                        CreateSimpleBlock()));
                    return level;
                }
            case 7: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-70, -10), new Pair<int>(28, 1), CreateImpenetrableBlock(
                        (c) => WallExtension.Right | WallExtension.Left
                        ), new Pair<float>(TIGHT_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-15, 20), new Pair<int>(5, 4), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));

                    level.Blocks.AddRange(CreateBlocks(new Vector2(-46, -25), new Pair<int>(3, 2), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-6, -25), new Pair<int>(3, 2), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(34, -25), new Pair<int>(3, 2), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));

                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(-40, -10 - TIGHT_Y_PADDING), new Vector2(-40, -10 + TIGHT_Y_PADDING)));
                    level.Blocks.AddRange(CreateTeleportBlocks(new Vector2(40, -10 - TIGHT_Y_PADDING), new Vector2(40, -10 + TIGHT_Y_PADDING)));
                    return level;
                }
            case 8: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 20), new Pair<int>(12, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-85, -10), new Pair<int>(14, 1), CreateImpenetrableMovingBlock(4, 
                        (x, y) => new List<Vector2> {
                            new Vector2(x,y),
                            new Vector2(x+20,y),
                        },
                        (c) => (c.XIndex == 13) ? WallExtension.Left : WallExtension.Right | WallExtension.Left
                        ), new Pair<float>(TIGHT_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(10, -10), new Pair<int>(13, 1), CreateImpenetrableMovingBlock(4,
                        (x, y) => new List<Vector2> {
                                                new Vector2(x,y),
                                                new Vector2(x+20,y),
                        },
                        (c) => (c.XIndex == 0) ? WallExtension.Right : WallExtension.Right | WallExtension.Left
                        ), new Pair<float>(TIGHT_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-80, -20), new Pair<int>(10, 1), CreateImpenetrableMovingBlock(5,
                        (x, y) => new List<Vector2> {
                                                new Vector2(x,y),
                                                new Vector2(x+15,y),
                        },
                        (c) => (c.XIndex == 9) ? WallExtension.Left : WallExtension.Right | WallExtension.Left
                        ), new Pair<float>(TIGHT_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-1, -20), new Pair<int>(15, 1), CreateImpenetrableMovingBlock(5,
                        (x, y) => new List<Vector2> {
                                                new Vector2(x,y),
                                                new Vector2(x+15,y),
                        },
                        (c) => (c.XIndex == 0) ? WallExtension.Right : WallExtension.Right | WallExtension.Left
                        ), new Pair<float>(TIGHT_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(0, -5), new Pair<int>(1, 4), CreateImpenetrableMovingBlock(5,
                        (x, y) => new List<Vector2> {
                                                                    new Vector2(x,y),
                                                                    new Vector2(x,y-10),
                        },
                        (c) =>
                        {
                            if (c.YIndex == 0) {
                                return WallExtension.Up;
                            }
                            if (c.YIndex == 3) {
                                return WallExtension.Down;
                            }
                            return WallExtension.Up | WallExtension.Down;
                        }
                        ), new Pair<float>(TIGHT_X_PADDING, TIGHT_Y_PADDING)));
                    return level;
                }
            case 9: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-30, 10), new Pair<int>(10, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    int xsize = 14;
                    int ysize = 6;
                    float x1 = -42;
                    float x2 = -42 + LOOSE_X_PADDING * (xsize - 1);
                    float y1 = 30;
                    float y2 = 30 + -LOOSE_X_PADDING * (ysize - 1);
                    Vector2 p1 = new Vector2(x1, y1);
                    Vector2 p2 = new Vector2(x2, y1);
                    Vector2 p3 = new Vector2(x2, y2);
                    Vector2 p4 = new Vector2(x1, y2);
                    level.Blocks.AddRange(CreateBlocks(p4, new Pair<int>(xsize, ysize), CreateImpenetrableMovingBlock(20, (x, y) =>
                        {
                            /*
                         *              y1
                         *    p1 - - - - - - - - - - p2
                         *    |                       |
                         *  x1|                     x2|
                         *    |         y2            |
                         *    p4 - -  - - -  - - - - p3
                         */
                        if (Mathf.RoundToInt(x) == Mathf.RoundToInt(x1)) {
                            return new List<Vector2> {
                                new Vector2(x,y),
                                p1,
                                p2,
                                p3,
                                p4,
                            };
                        }
                            if (Mathf.RoundToInt(x) == Mathf.RoundToInt(x2)) {
                                return new List<Vector2> {
                                    new Vector2(x,y),
                                    p3,
                                    p4,
                                    p1,
                                    p2
                                };
                            }
                            if (Mathf.RoundToInt(y) == Mathf.RoundToInt(y1)) {
                                return new List<Vector2> {
                                    new Vector2(x,y),
                                    p2,
                                    p3,
                                    p4,
                                    p1,
                                };
                            }
                            return new List<Vector2> {
                                new Vector2(x,y),
                                p4,
                                p1,
                                p2,
                                p3,
                            };
                        }), new Pair<float>(LOOSE_X_PADDING, LOOSE_X_PADDING),
                    (ix, iy) => (ix == 0 || iy == 0 || iy == ysize - 1)));
                    return level;
                }
            case 10: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-54, 6), new Pair<int>(19, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-54, 3), new Pair<int>(19, 1), CreateSimpleBlock(2, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-54, 0), new Pair<int>(19, 1), CreateSimpleBlock(3, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-54, -3), new Pair<int>(19, 1), CreateSimpleBlock(4, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-54, -6), new Pair<int>(19, 1),
                        CreateImpenetrableBlock(
                            (c) => {
                                WallExtension wall = WallExtension.None;
                                if (c.XIndex > 0) {
                                    wall |= WallExtension.Left;
                                }
                                if (c.XIndex < 18) {
                                    wall |= WallExtension.Right;
                                }
                                return wall;
                            }
                        ),
                    new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            default:
                throw new Exception("Level " + levelNo + " of set C not impmemented");
        }
    }

    private FLevel SetD(int levelNo) {
        switch (levelNo) {
            case 1: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 20), new Pair<int>(12, 5), CreateSimpleMovingBlock(3, (x,y) =>
                    new List<Vector2> {
                        new Vector2(x-5, y),
                        new Vector2(x+5, y),
                    })
                    , new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            case 2: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-50, 30), new Pair<int>(5, 5), CreateSimpleMovingBlock(8, (x, y) => new List<Vector2> {
                        new Vector2(x,y),
                        new Vector2(x+70,y),
                        new Vector2(x+70,y-50),
                        new Vector2(x,y-50),
                    }), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-50, 30), new Pair<int>(5, 5), CreateSimpleMovingBlock(8, (x, y) => new List<Vector2> {
                        new Vector2(x+70,y-50),
                        new Vector2(x,y-50),
                        new Vector2(x,y),
                        new Vector2(x+70,y),
                    }), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            case 3: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-30, 20), new Pair<int>(50, 1), CreateSimpleMovingBlock(null, (x, y) => {
                        var perm = new List<Vector2> {
                            new Vector2(UnityEngine.Random.Range(-50,-25),UnityEngine.Random.Range(30, 40)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(30,40)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(-10,0)),
                            new Vector2(UnityEngine.Random.Range(-50,25),UnityEngine.Random.Range(-10,0)),
                        };
                        var start = UnityEngine.Random.Range(0, 4);
                        return new List<Vector2> {
                            perm[(start + 0) % 4],
                            perm[(start + 1) % 4],
                            perm[(start + 2) % 4],
                            perm[(start + 3) % 4],
                        };
                    }, coloring: ColorBlockRandom), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-30, 20), new Pair<int>(5, 1), CreateImpenetrableMovingBlock(null, (x, y) => {
                        var perm = new List<Vector2> {
                            new Vector2(UnityEngine.Random.Range(-50,-25),UnityEngine.Random.Range(22, 28)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(22,28)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(-15,-11)),
                            new Vector2(UnityEngine.Random.Range(-50,25),UnityEngine.Random.Range(-15,-11)),
                        };
                        var start = UnityEngine.Random.Range(0, 4);
                        return new List<Vector2> {
                            perm[(start + 0) % 4],
                            perm[(start + 1) % 4],
                            perm[(start + 2) % 4],
                            perm[(start + 3) % 4],
                        };
                    }), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            case 4: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-12, 10), new Pair<int>(5, 7), CreateSimpleBlock(2), new Pair<float>(LOOSE_X_PADDING, TIGHT_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-42, -10), new Pair<int>(3, 4), CreateSimpleMovingBlock(
                        15,
                        (x,y) => new List<Vector2> {
                            new Vector2(x, y),
                            new Vector2(x, y+40),
                            new Vector2(x+84, y+40),
                            new Vector2(x+84, y),
                        }
                        ), new Pair<float>(LOOSE_X_PADDING, TIGHT_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-42, -10), new Pair<int>(3, 4), CreateSimpleMovingBlock(
                        15,
                        (x, y) => new List<Vector2> {                            
                            new Vector2(x, y+40),
                            new Vector2(x+84, y+40),
                            new Vector2(x+84, y),
                            new Vector2(x, y),
                        }
                        ), new Pair<float>(LOOSE_X_PADDING, TIGHT_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-42, -10), new Pair<int>(3, 4), CreateSimpleMovingBlock(
                        15,
                        (x, y) => new List<Vector2> {
                            new Vector2(x+84, y+40),
                            new Vector2(x+84, y),
                            new Vector2(x, y),
                            new Vector2(x, y+40),
                        }
                        ), new Pair<float>(LOOSE_X_PADDING, TIGHT_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-42, -10), new Pair<int>(3, 4), CreateSimpleMovingBlock(
                        15,
                        (x, y) => new List<Vector2> {
                            new Vector2(x+84, y),
                            new Vector2(x, y),
                            new Vector2(x, y+40),
                            new Vector2(x+84, y+40),
                        }
                        ), new Pair<float>(LOOSE_X_PADDING, TIGHT_Y_PADDING)));
                    return level;
                }
            case 5: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    ShootingIntervalParams shootingParams = new ShootingIntervalParams(intervalRandomness: new Pair<int>(0, 2), offsetRandomness: new Pair<int>(0, 4));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 20), new Pair<int>(12, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING))); 
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-48, 45), new Pair<int>(5, 1), CreateShootingBlock((c) => shootingParams), new Pair<float>(24, 1)));
                    return level;
                }
            case 6: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    ShootingIntervalParams shootingParams = new ShootingIntervalParams(intervalRandomness: new Pair<int>(0, 2), offsetRandomness: new Pair<int>(0, 4));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 20), new Pair<int>(12, 6), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                        (ix, iy) => !((ix == 3 || ix == 8) && (iy == 1 || iy == 4)),
                        CreateShootingBlock((c) => shootingParams)));
                    return level;
                }
            case 7: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    ShootingIntervalParams shootingParams = new ShootingIntervalParams(intervalRandomness: new Pair<int>(0, 2), offsetRandomness: new Pair<int>(0, 4));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-12, 10), new Pair<int>(5, 7), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, TIGHT_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, -6), new Pair<int>(1, 1), CreateMovingShootingBlock(
                    15,
                    (x, y) => new List<Vector2> {
                            new Vector2(x, y),
                            new Vector2(x, y+40),
                            new Vector2(x+72, y+40),
                            new Vector2(x+72, y),
                    },
                    (c) => shootingParams,
                    2
                    ), new Pair<float>(LOOSE_X_PADDING, TIGHT_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, -6), new Pair<int>(1, 1), CreateMovingShootingBlock(
                        15,
                        (x, y) => new List<Vector2> {
                            new Vector2(x, y+40),
                            new Vector2(x+72, y+40),
                            new Vector2(x+72, y),
                            new Vector2(x, y),
                        },
                        (c) => shootingParams,
                        2
                    ), new Pair<float>(LOOSE_X_PADDING, TIGHT_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, -6), new Pair<int>(1, 1), CreateMovingShootingBlock(
                        15,
                        (x, y) => new List<Vector2> {
                                new Vector2(x+72, y+40),
                                new Vector2(x+72, y),
                                new Vector2(x, y),
                                new Vector2(x, y+40),
                        },
                        (c) => shootingParams,
                        2
                    ), new Pair<float>(LOOSE_X_PADDING, TIGHT_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, -6), new Pair<int>(1, 1), CreateMovingShootingBlock(
                        15,
                        (x, y) => new List<Vector2> {
                                new Vector2(x+72, y),
                                new Vector2(x, y),
                                new Vector2(x, y+40),
                                new Vector2(x+72, y+40),
                        },
                        (c) => shootingParams,
                        2
                    ), new Pair<float>(LOOSE_X_PADDING, TIGHT_Y_PADDING)));
                    return level;
                }
            case 8: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, -20), new Pair<int>(21, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 0), new Pair<int>(21, 1), CreateSimpleBlock(2, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 20), new Pair<int>(21, 1), CreateSimpleBlock(3, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    ShootingIntervalParams shootingParams = new ShootingIntervalParams(intervalRandomness: new Pair<int>(0, 2), offsetRandomness: new Pair<int>(0, 4));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-20, 30), new Pair<int>(2, 1), CreateMovingShootingBlock(20,
                        (x,y) => new List<Vector2> {
                            new Vector2(x-20,y-2),
                            new Vector2(x-20,y+2),
                            new Vector2(x+20,y+2),
                            new Vector2(x+20,y-2),                        },
                        (c) => shootingParams), new Pair<float>(40, 1)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-20, 10), new Pair<int>(2, 1), CreateMovingShootingBlock(20,
                        (x, y) => new List<Vector2> {
                            new Vector2(x-20,y-2),
                            new Vector2(x-20,y+2),
                            new Vector2(x+20,y+2),
                            new Vector2(x+20,y-2),                        },
                        (c) => shootingParams), new Pair<float>(40, 1)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-20, -10), new Pair<int>(2, 1), CreateMovingShootingBlock(20,
                        (x, y) => new List<Vector2> {
                            new Vector2(x-20,y-2),
                            new Vector2(x-20,y+2),
                            new Vector2(x+20,y+2),
                            new Vector2(x+20,y-2),                        },
                        (c) => shootingParams), new Pair<float>(40, 1)));
                    return level;
                }
            case 9: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    ShootingIntervalParams shootingParams = new ShootingIntervalParams(intervalRandomness: new Pair<int>(0, 2), offsetRandomness: new Pair<int>(0, 4));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 20), new Pair<int>(12, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-48, 45), new Pair<int>(5, 1), CreateShootingBlock((c) => shootingParams), new Pair<float>(24, 1)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(36, 26), new Pair<int>(6, 1), CreateImpenetrableBlock(
                        (c) => (c.XIndex == 0) ? WallExtension.Right : WallExtension.Left | WallExtension.Right
                        ), new Pair<float>(LOOSE_X_PADDING, TIGHT_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-72, 26), new Pair<int>(6, 1), CreateImpenetrableBlock(
                        (c) => (c.XIndex == 0) ? WallExtension.Right : WallExtension.Left | WallExtension.Right
                        ), new Pair<float>(LOOSE_X_PADDING, TIGHT_Y_PADDING)));
                    return level;
                }
            case 10: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    ShootingIntervalParams shootingParams = new ShootingIntervalParams(intervalRandomness: new Pair<int>(0, 2), offsetRandomness: new Pair<int>(0, 4));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-30, 20), new Pair<int>(30, 1), CreateSimpleMovingBlock(null, (x, y) => {
                        var perm = new List<Vector2> {
                            new Vector2(UnityEngine.Random.Range(-50,-25),UnityEngine.Random.Range(30, 40)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(30,40)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(-10,0)),
                            new Vector2(UnityEngine.Random.Range(-50,25),UnityEngine.Random.Range(-10,0)),
                        };
                        var start = UnityEngine.Random.Range(0, 4);
                        return new List<Vector2> {
                            perm[(start + 0) % 4],
                            perm[(start + 1) % 4],
                            perm[(start + 2) % 4],
                            perm[(start + 3) % 4],
                        };
                    }, coloring: ColorBlockRandom), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-30, 20), new Pair<int>(20, 1), CreateSimpleMovingBlock(null, (x, y) => {
                        var perm = new List<Vector2> {
                            new Vector2(UnityEngine.Random.Range(-50,-25),UnityEngine.Random.Range(30, 40)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(30,40)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(-10,0)),
                            new Vector2(UnityEngine.Random.Range(-50,25),UnityEngine.Random.Range(-10,0)),
                        };
                        var start = UnityEngine.Random.Range(0, 4);
                        return new List<Vector2> {
                            perm[(start + 0) % 4],
                            perm[(start + 1) % 4],
                            perm[(start + 2) % 4],
                            perm[(start + 3) % 4],
                        };
                    }, coloring: ColorBlockRandom, maxHp: 2), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-30, 20), new Pair<int>(7, 1), CreateMovingShootingBlock(null, (x, y) => {
                        var perm = new List<Vector2> {
                            new Vector2(UnityEngine.Random.Range(-50,-25),UnityEngine.Random.Range(30, 40)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(30,40)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(-10,0)),
                            new Vector2(UnityEngine.Random.Range(-50,25),UnityEngine.Random.Range(-10,0)),
                        };
                        var start = UnityEngine.Random.Range(0, 4);
                        return new List<Vector2> {
                            perm[(start + 0) % 4],
                            perm[(start + 1) % 4],
                            perm[(start + 2) % 4],
                            perm[(start + 3) % 4],
                        };
                    }, (c) => shootingParams), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-30, 20), new Pair<int>(7, 1), CreateMovingShootingBlock(null, (x, y) => {
                        var perm = new List<Vector2> {
                            new Vector2(UnityEngine.Random.Range(-50,-25),UnityEngine.Random.Range(30, 40)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(30,40)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(-10,0)),
                            new Vector2(UnityEngine.Random.Range(-50,25),UnityEngine.Random.Range(-10,0)),
                        };
                        var start = UnityEngine.Random.Range(0, 4);
                        return new List<Vector2> {
                            perm[(start + 0) % 4],
                            perm[(start + 1) % 4],
                            perm[(start + 2) % 4],
                            perm[(start + 3) % 4],
                        };
                    }, (c) => shootingParams, 2), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-30, 20), new Pair<int>(5, 1), CreateImpenetrableMovingBlock(null, (x, y) => {
                        var perm = new List<Vector2> {
                            new Vector2(UnityEngine.Random.Range(-50,-25),UnityEngine.Random.Range(22, 28)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(22,28)),
                            new Vector2(UnityEngine.Random.Range(25,50),UnityEngine.Random.Range(-15,-11)),
                            new Vector2(UnityEngine.Random.Range(-50,25),UnityEngine.Random.Range(-15,-11)),
                        };
                        var start = UnityEngine.Random.Range(0, 4);
                        return new List<Vector2> {
                            perm[(start + 0) % 4],
                            perm[(start + 1) % 4],
                            perm[(start + 2) % 4],
                            perm[(start + 3) % 4],
                        };
                    }), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            default:
                throw new Exception("Level " + levelNo + " of set D not impmemented");
        }
    }

    private FLevel SetE(int levelNo) {
        switch (levelNo) {
            case 1: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 36), new Pair<int>(21, 1), CreateSimpleBlock(4, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 24), new Pair<int>(21, 1), CreateSimpleBlock(2, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 12), new Pair<int>(21, 1), CreateSimpleBlock(coloring: ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 0), new Pair<int>(21, 1), CreateSimpleBlock(2, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, -12), new Pair<int>(21, 1), CreateSimpleBlock(4, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            case 2: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    ShootingIntervalParams shootingParams = new ShootingIntervalParams(intervalRandomness: new Pair<int>(1, 5), offsetRandomness: new Pair<int>(0, 4));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-33, 10), new Pair<int>(3, 2), CreateShootingBlock(
                        (c) => shootingParams
                        ), new Pair<float>(30, 10)));
                    int xsize = 14;
                    int ysize = 6;
                    float x1 = -42;
                    float x2 = -42 + LOOSE_X_PADDING * (xsize - 1);
                    float y1 = 30;
                    float y2 = 30 + -LOOSE_X_PADDING * (ysize - 1);
                    Vector2 p1 = new Vector2(x1, y1);
                    Vector2 p2 = new Vector2(x2, y1);
                    Vector2 p3 = new Vector2(x2, y2);
                    Vector2 p4 = new Vector2(x1, y2);
                    level.Blocks.AddRange(CreateBlocks(p4, new Pair<int>(xsize, ysize), CreateSimpleMovingBlock(20, (x, y) =>
                        {
                            /*
                         *              y1
                         *    p1 - - - - - - - - - - p2
                         *    |                       |
                         *  x1|                     x2|
                         *    |         y2            |
                         *    p4 - -  - - -  - - - - p3
                         */
                        if (Mathf.RoundToInt(x) == Mathf.RoundToInt(x1)) {
                            return new List<Vector2> {
                                new Vector2(x,y),
                                p1,
                                p2,
                                p3,
                                p4,
                            };
                        }
                            if (Mathf.RoundToInt(x) == Mathf.RoundToInt(x2)) {
                                return new List<Vector2> {
                                    new Vector2(x,y),
                                    p3,
                                    p4,
                                    p1,
                                    p2
                                };
                            }
                            if (Mathf.RoundToInt(y) == Mathf.RoundToInt(y1)) {
                                return new List<Vector2> {
                                    new Vector2(x,y),
                                    p2,
                                    p3,
                                    p4,
                                    p1,
                                };
                            }
                            return new List<Vector2> {
                                new Vector2(x,y),
                                p4,
                                p1,
                                p2,
                                p3,
                            };
                        }, coloring: ColorBlockRandom, maxHp: 4), new Pair<float>(LOOSE_X_PADDING, LOOSE_X_PADDING),
                    (ix, iy) => (ix == 0 || ix == xsize - 1 || iy == 0 || iy == ysize - 1)));
                    return level;
                }
            case 3: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 10), new Pair<int>(11, 11), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                        (ix, iy) => iy > ix,
                        (c) => c.XIndex == c.YIndex ? CreateSimpleBlock(4)(c) : null));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(0, 10), new Pair<int>(11, 11), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING),
                        (ix, iy) => (10 - iy) < ix,
                        (c) => c.XIndex == (10 - c.YIndex) ? CreateSimpleBlock(4)(c) : null));
                    return level;
                }
            case 4: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 20), new Pair<int>(12, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    foreach (float f in new float[] { -36, -18, 0, 18, 30}) {
                        level.Blocks.AddRange(CreateBlocks(new Vector2(f, 40), new Pair<int>(1, 1), CreateImpenetrableMovingBlock(
                            Math.Abs(f % 14) + 10, (x, y) => new List<Vector2> {
                            new Vector2(x, y),
                            new Vector2(x, y-30),
                            }
                       ), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    }
                    foreach (float f in new float[] { 20, 26, 32 }) {
                        level.Blocks.AddRange(CreateBlocks(new Vector2(-40, f), new Pair<int>(1, 1), CreateImpenetrableMovingBlock(
                            Math.Abs(f % 14) + 10, (x, y) => new List<Vector2> {
                            new Vector2(x, y),
                            new Vector2(x+76, y),
                            }
                       ), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    }
                    return level;
                }
            case 5: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 20), new Pair<int>(13, 5), CreateSimpleMovingBlock(1,
                        (x,y) => new List<Vector2> {
                            new Vector2(x,y),
                            new Vector2(x,y-60),
                        }
                        ), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            case 6: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 20), new Pair<int>(12, 5), CreateSimpleBlock(), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 35), new Pair<int>(3, 4), CreateSimpleMovingBlock(1,
                        (x,y) => new List<Vector2> {
                            new Vector2(x,y),
                            new Vector2(x,y-80),
                        }
                        ), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(42, 35), new Pair<int>(3, 4), CreateSimpleMovingBlock(1,
                        (x, y) => new List<Vector2> {
                            new Vector2(x,y),
                            new Vector2(x,y-80),
                        }
                        ), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            case 7: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 20), new Pair<int>(21, 1), CreateSimpleBlock(1, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, -20), new Pair<int>(21, 1), CreateSimpleBlock(1, ColorBlockByColumn), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-60, 35), new Pair<int>(4, 5), CreateSimpleMovingBlock(1,
                    (x, y) => new List<Vector2> {
                        new Vector2(x,y),
                        new Vector2(x+98,y),
                        new Vector2(x+98,y-40),
                        new Vector2(x,y-40),
                        new Vector2(x,y-80),
                        new Vector2(x+98,y-80),
                    },
                    4), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            case 8: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-36, 30), new Pair<int>(12, 5), CreateSimpleMovingBlock(2,
                        (x,y) => new List<Vector2> {
                            new Vector2(x,y),
                            new Vector2(x,y-70),
                        }
                        ), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-63, -20), new Pair<int>(8, 8), CreateImpenetrableBlock(), new Pair<float>(18, 8)));
                    return level;
                }
            case 9: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-63, -20), new Pair<int>(8, 6), CreateSimpleBlock(), new Pair<float>(18, 8)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-70, 30), new Pair<int>(28, 1), CreateImpenetrableMovingBlock(10,
                        (x,y) => new List<Vector2> {
                            new Vector2(x,y),
                            new Vector2(x,y-60),
                        },
                        (c) => WallExtension.Right | WallExtension.Left
                        ), new Pair<float>(TIGHT_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            case 10: {
                FLevel level = new FLevel() { Blocks = new List<FBlock>() };
                    ShootingIntervalParams shootingParams = new ShootingIntervalParams(intervalRandomness: new Pair<int>(8, 22), offsetRandomness: new Pair<int>(3, 22));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-50, -35), new Pair<int>(3, 2), CreateSimpleBlock(2), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-6, -35), new Pair<int>(3, 2), CreateSimpleBlock(2), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(32, -35), new Pair<int>(3, 2), CreateSimpleBlock(2), new Pair<float>(LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    level.Blocks.AddRange(CreateBlocks(new Vector2(-50, 35), new Pair<int>(7, 5), CreateMovingShootingBlock(2,
                        (x, y) => new List<Vector2> {
                            new Vector2(x, y),
                            new Vector2(x+70, y),
                            new Vector2(x+70, y-25),
                            new Vector2(x, y-25),
                            new Vector2(x, y-50),
                            new Vector2(x+70, y-50),
                            new Vector2(x+70, y-75),
                            new Vector2(x, y-75),
                        }, (c) => shootingParams
                        ), new Pair<float>(VERY_LOOSE_X_PADDING, LOOSE_Y_PADDING)));
                    return level;
                }
            default:
                throw new Exception("Level " + levelNo + " of set D not impmemented");
        }
    }

    
}