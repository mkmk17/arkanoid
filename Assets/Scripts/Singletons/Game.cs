﻿using UnityEngine;
using UnityEngine.SceneManagement;

//Class responsible for things that are neither pure menu responsibility nor pure gameplay responsibility.
public class Game : Singleton<Game> {
    protected Game() {
    }

    public void StartSet(string setName)
    {
        SetTracker.Instance.GetSetDefinition(setName);
        LoadingScreen.InitialLoading = false;
        SetTracker.Instance.Score = 0;
        SetTracker.Instance.Life = 3;
        SetTracker.Instance.TotalTime = 0;
        SetTracker.Instance.CurrentLevel = new LevelInfo(setName);
        SceneManager.LoadScene("Loading");
    }

    public void EndGamePlay() {
        SoundPlayer.Instance.StopMusic();
        SceneManager.LoadScene("Set Select");
    }
}
