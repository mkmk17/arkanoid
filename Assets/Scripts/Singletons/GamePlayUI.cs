﻿using UnityEngine;
using UnityEngine.UI;

//Class responsible for updating UI during the game.
public class GamePlayUI : WeakSingleton<GamePlayUI> {
    protected GamePlayUI() {
    }

    private LocalizedText gameOverText;
    private LocalizedText winText;
    private LocalizedText lifeText;
    private LocalizedText scoreText;
    private LocalizedText timeText;
    private LocalizedText pauseText;
    private GameObject gamePlayMenu;

    // Use this for initialization
    void Start () {
        gameOverText = GameObject.FindWithTag("GameOverUI").GetComponent<LocalizedText>();
        winText = GameObject.FindWithTag("WinTextUI").GetComponent<LocalizedText>();
        pauseText = GameObject.FindWithTag("PauseUI").GetComponent<LocalizedText>();
        scoreText = GameObject.FindWithTag("Score Meter").GetComponent<LocalizedText>();
        lifeText = GameObject.FindWithTag("Life Meter").GetComponent<LocalizedText>();
        timeText = GameObject.FindWithTag("Time Meter").GetComponent<LocalizedText>();
        gamePlayMenu = GameObject.FindWithTag("GamePlayMenu");
        pauseText.gameObject.SetActive(false);
        gameOverText.gameObject.SetActive(false);
        winText.gameObject.SetActive(false);
        gamePlayMenu.SetActive(false);
    }

    private void Update() {
        if (GamePlay.Instance.GameOver) {
            gamePlayMenu.SetActive(true);
            pauseText.gameObject.SetActive(false);
            if (GamePlay.Instance.LevelWon) {
                winText.gameObject.SetActive(true);
                winText.SetVal(
                    LevelCreator.Instance.IsLastLevel() ? "setCompleted" :
                    PlayerPrefs.GetInt("Controls", 1) == 1 ? "levelCompletedMouse" :
                    "levelCompletedKeyboard"
                );
                if (LevelCreator.Instance.IsLastLevel()) {
                    winText.SetParam(0, SetTracker.Instance.RealScore.ToString());
                }
            }
            else {
                gameOverText.gameObject.SetActive(true);
            }
        }
        scoreText.SetParam(0, GamePlay.Instance.Score.ToString());
        lifeText.SetParam(0, GamePlay.Instance.Life.ToString());
        timeText.SetParam(0, SetTracker.Instance.TimeBonus.ToString());
    }

    public void Pause() {
        gamePlayMenu.SetActive(true);
        pauseText.gameObject.SetActive(true);
    }

    public void UnPause() {
        gamePlayMenu.SetActive(false);
        pauseText.gameObject.SetActive(false);
    }
}
