﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

//Class responsible for managing bonuses during the game.
public class BonusManager : BaseManager<Bonus> {
    protected BonusManager() {
    }

    public new static BonusManager Instance {
        get {
            return BaseManager<Bonus>.Instance as BonusManager;
        }
    }

    private GameObject autoBonusSpawn;
    private List<GameObject> bonusPrefabs;
    private List<float> bonusProbab;
    //List of bonuses that are currently active and are waiting to be finished.
    private List<TimedBonus> currentTimeBonuses;

    private const int AUTO_BONUS_RATE = 30;
    private int autoBonusClock;

    protected void Start () {
        autoBonusSpawn = GameObject.FindGameObjectWithTag("AutoBonusSpawn");
        currentTimeBonuses = new List<TimedBonus>();
        autoBonusClock = 1;
        InitBonuses();
        InvokeRepeating("UpdateTimeBonuses", 0, 1);
        InvokeRepeating("UpdateAutoBonuses", 0, 1);
    }

    void InitBonuses() {
        bonusPrefabs = new List<GameObject>();
        bonusProbab = new List<float>();
        bonusPrefabs.Add((GameObject)Resources.Load(@"Prefabs\Bonuses\ExplodingBallBonus", typeof(GameObject)));
        bonusProbab.Add(0.1f); //0.1
        bonusPrefabs.Add((GameObject)Resources.Load(@"Prefabs\Bonuses\StrongBallBonus", typeof(GameObject)));
        bonusProbab.Add(0.2f); //0.1
        bonusPrefabs.Add((GameObject)Resources.Load(@"Prefabs\Bonuses\FasterBallBonus", typeof(GameObject)));
        bonusProbab.Add(0.35f); //0.15
        bonusPrefabs.Add((GameObject)Resources.Load(@"Prefabs\Bonuses\SlowerBallBonus", typeof(GameObject)));
        bonusProbab.Add(0.5f); //0.15
        bonusPrefabs.Add((GameObject)Resources.Load(@"Prefabs\Bonuses\NewBallBonus", typeof(GameObject)));
        bonusProbab.Add(0.85f); //0.35
        bonusPrefabs.Add((GameObject)Resources.Load(@"Prefabs\Bonuses\NewLifeBonus", typeof(GameObject)));
        bonusProbab.Add(0.9f); //0.05
        bonusPrefabs.Add((GameObject)Resources.Load(@"Prefabs\Bonuses\HarmfulBonus", typeof(GameObject)));
        bonusProbab.Add(1000); //0.1	
    }

    public void DropAutoBonus() {
        elements.Add(Instantiate(bonusPrefabs[4], autoBonusSpawn.transform.position, Quaternion.identity, GetBonusParent()).GetComponent<Bonus>());
    }

    public void DropBonus(Vector2 position) {
        if (GamePlay.Instance.GameOver) {
            //It was last block destroyed. Let it go.
            return;
        }
        GameObject instantiateBonus = null;
        float r = Random.value;
        for (int i = 0; i <= bonusPrefabs.Count; i++) {
            if (r < bonusProbab[i]) {
                instantiateBonus = Instantiate(bonusPrefabs[i], position, Quaternion.identity, GetBonusParent());
                break;
            }
        }
        instantiateBonus = instantiateBonus ?? Instantiate(bonusPrefabs[bonusPrefabs.Count - 1], position, Quaternion.identity, GetBonusParent());
        elements.Add(instantiateBonus.GetComponent<Bonus>());
    }

    public void DestroyBonus(Bonus bonus) {
        if (RemoveElement(bonus)) {
            Destroy(bonus.gameObject);
        }
    }

    public void AddTimeBonus(TimedBonus bonus) {
        currentTimeBonuses = currentTimeBonuses.Where(x => (x.TimedBonusType != bonus.TimedBonusType)).ToList();
        currentTimeBonuses.Add(bonus);
    } 

    [UsedImplicitly]
    private void UpdateTimeBonuses() {
        for (int i = 0; i < currentTimeBonuses.Count; i++) {
            if (currentTimeBonuses[i].EndTime <= GameTime.Instance.TotalTime) {
                currentTimeBonuses[i].ActionToDo();
                currentTimeBonuses[i] = null;
            }
        }
        currentTimeBonuses = currentTimeBonuses.Where(x => x != null).ToList();
    }

    [UsedImplicitly]
    private void UpdateAutoBonuses() {
        if (paused)
        {
            return;
        }
        autoBonusClock = (autoBonusClock + 1) % AUTO_BONUS_RATE;
        if (autoBonusClock == 0) {
            DropAutoBonus();
        }
    }


    public override void ClearLevel() {
        base.ClearLevel();
        currentTimeBonuses.Clear();
    }

    public static Transform GetBonusParent() {
        return GameObject.FindGameObjectWithTag("BonusSpawn").transform;
    }
}
