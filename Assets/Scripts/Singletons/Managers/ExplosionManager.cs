﻿using System.Collections.Generic;
using UnityEngine;

//Class is managing explosions in the gameplay.
public class ExplosionManager : BaseManager<BaseExplosion> {

    protected ExplosionManager() {
    }

    public new static ExplosionManager Instance {
        get {
            return BaseManager<BaseExplosion>.Instance as ExplosionManager;
        }
    }

    private GameObject lightDamage;
    private GameObject destroyed;
    private GameObject ballExplosion;

	void Awake () {
        lightDamage = (GameObject)Resources.Load(@"Prefabs\Particles\LightDamage", typeof(GameObject));
        destroyed = (GameObject)Resources.Load(@"Prefabs\Particles\Destroyed", typeof(GameObject));
        ballExplosion = (GameObject)Resources.Load(@"Prefabs\Particles\BallExplosion", typeof(GameObject));
    }

    public void CreateExplosion(Vector3 position) {
        GameObject explosion = Instantiate(ballExplosion, position, Quaternion.identity, GetExplosionParent());
        if (GamePlay.Instance.GameOver) {
            //Explosion caused game over
            Destroy(explosion.gameObject);
        }
        else {
            AddElement(explosion.GetComponent<BaseExplosion>());
        }
    }

    private void CreateParticleSystemExplosion(GameObject prefab, Vector3 position, Color color) {
        GameObject ld = Instantiate(prefab, position, Quaternion.identity, GetExplosionParent());
        if (GamePlay.Instance.GameOver) {
            //Explosion caused game over
            Destroy(ld.gameObject);
        }
        else {
            AddElement(ld.GetComponent<BaseExplosion>());
            ld.GetComponent<ParticleExplosion>().Color = color;
        }
    }

    public void CreateLightDamage(Vector3 position, Color color) {
        CreateParticleSystemExplosion(lightDamage, position, color);
    }

    public void BlockDestruction(Vector3 position, Color color) {
        CreateParticleSystemExplosion(destroyed, position, color);
    }

    public Transform GetExplosionParent() {
        return GameObject.FindGameObjectWithTag("Explosion Spawn").transform;
    }
}
