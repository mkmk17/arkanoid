﻿using System;
using System.Collections.Generic;
using UnityEngine;

//Class responsible for managing balls during the game.
public abstract class BaseManager<T> : WeakSingleton<BaseManager<T>> where T : PausableBehaviour
{
    protected bool paused = false;
    protected List<T> elements = new List<T>();
    public virtual int ElementNo
    {
        get { return elements.Count; }
    }

    public virtual void Pause()
    {
        paused = true;
        foreach (var element in elements)
        {
            element.Pause();
        }
    }

    public virtual void UnPause()
    {
        paused = false;
        foreach (var element in elements) {
            element.UnPause();
        }
    }

    public virtual void ClearLevel() {
        foreach (var element in elements)
        {
            Destroy(element);
        }
        elements.Clear();
    }

    public virtual bool RemoveElement(T elem)
    {
        //Debug.Log("We're removing " + elem.GetType() + " elements type is " + elements.GetType());
        Destroy(elements.FindLast(e => e == elem));
        return elements.Remove(elem);
    }

    public virtual void AddElement(T elem) {
        //Debug.Log("We're adding " + elem.GetType() + " elements type is " + elements.GetType());
        elements.Add(elem);
    }
}