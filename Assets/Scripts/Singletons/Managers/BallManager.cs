﻿using System;
using System.Collections.Generic;
using UnityEngine;

//Class responsible for managing balls during the game.
public class BallManager : BaseManager<Ball> {
    protected BallManager() {
    }

    public new static BallManager Instance {
        get
        {
            return BaseManager<Ball>.Instance as BallManager;
        }
    }

    private GameObject ballPrefab;
    
    //Two spawnpoints for ball. One near the paddle. Other one for bonus spawning a new ball.
    public GameObject PaddleSpawnPoint;

    void Start () {
        PaddleSpawnPoint = GameObject.FindWithTag("PaddleSpawn");
        ballPrefab = (GameObject)Resources.Load(@"Prefabs\Ball", typeof(GameObject));
        SpawnBall();
    }

    //Spawn ball near paddle motionless
    public void SpawnBall() {
        Vector3 position = new Vector3(PaddleSpawnPoint.transform.position.x, PaddleSpawnPoint.transform.position.y, 0);
        var ball = Instantiate(ballPrefab, position, Quaternion.identity, GetBallParent());
        ball.GetComponent<Ball>().Started = false;
        elements.Add(ball.GetComponent<Ball>());
    }

    //Spawn ball on another ball (new ball bonus)
    public void SpawnBall(Vector2 spawn) {
        Vector3 position = new Vector3(spawn.x, spawn.y, 0);
        var ball = Instantiate(ballPrefab, position, Quaternion.identity, GetBallParent());
        ball.GetComponent<Ball>().Started = true;
        elements.Add(ball.GetComponent<Ball>());
    }

    public void DestroyBall(Ball ball) {
        if (RemoveElement(ball)) {
            if (ElementNo <= 0) {
                GamePlay.Instance.LostLife();
            }
        }
    }

    public void BallMap(Action<Ball> action) {
        foreach (Ball ball in elements) {
            action(ball);
        }
    }

    public Transform GetBallParent() {
        return GameObject.FindGameObjectWithTag("BallSpawn").transform;
    }
}
