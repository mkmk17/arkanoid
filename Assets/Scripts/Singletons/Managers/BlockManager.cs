﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//Class responsible for managing blocks during the gameplay.
public class BlockManager : BaseManager<Block> {

    protected BlockManager()
    {

    }

    public new static BlockManager Instance {
        get {
            return BaseManager<Block>.Instance as BlockManager;
        }
    }


    //Number of blocks left that need to be destroyed to complete the level.
    private int blockCount;
    private List<Sprite> simpleBlocks, enemyBlocks;

    private void Awake() {
        blockCount = 0;
        simpleBlocks = new List<int>() { 0, 1, 2, 3 }.Select(index => (Sprite)Resources.Load(@"Sprites\Blocks\Block" + index, typeof(Sprite))).ToList();
        enemyBlocks = new List<int>() { 0, 1, 2, 3 }.Select(index => (Sprite)Resources.Load(@"Sprites\Blocks\Enemy" + index, typeof(Sprite))).ToList();
    }

    public Sprite SimpleBlockSprite(int index, bool simpleBlock) {
        return simpleBlock ? simpleBlocks[index] : enemyBlocks[index];
    }

    public void DestroyBlock(Block block) {
        if (RemoveElement(block)) {
            if (block is SimpleBlock) {
                blockCount--;
                if (blockCount <= 0) {
                    GamePlay.Instance.WinGame();
                }
            }
        }
    }

    public void CreateBlock(Block block) {
        AddElement(block);
        if (block is SimpleBlock) {
            blockCount++;
        }
    }

    public override void ClearLevel()
    {
    }
}
