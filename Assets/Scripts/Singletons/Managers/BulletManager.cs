﻿using System.Collections.Generic;
using UnityEngine;

//Class responsible form managing all bullets in the scene.
public class BulletManager : BaseManager<Bullet> {

    protected BulletManager() {
    }

    public new static BulletManager Instance {
        get {
            return BaseManager<Bullet>.Instance as BulletManager;
        }
    }

    //Initial speed magnitude.
    public const float INIT_BULLET_SPEED = 70;
    private GameObject player;
    GameObject bulletPrefab, enemyBulletPrefab;

    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        bulletPrefab = (GameObject)Resources.Load(@"Prefabs\Bullet", typeof(GameObject));
        enemyBulletPrefab = (GameObject)Resources.Load(@"Prefabs\EnemyBullet", typeof(GameObject));
    }

    //Spawning player bullets that destroys the ball.
    public void SpawnBulletsAfterBallsCollision(Vector3 collisionPlace) {
        GameObject bullet = Instantiate(bulletPrefab, collisionPlace, Quaternion.Euler(0, 0, 90), GetBulletParent());
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(INIT_BULLET_SPEED, 0);
        elements.Add(bullet.GetComponent<Bullet>());
        bullet = Instantiate(bulletPrefab, collisionPlace, Quaternion.Euler(0, 0, 90), GetBulletParent());
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(-INIT_BULLET_SPEED, 0);
        elements.Add(bullet.GetComponent<Bullet>());
        bullet = Instantiate(bulletPrefab, collisionPlace, Quaternion.identity, GetBulletParent());
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(0, INIT_BULLET_SPEED);
        elements.Add(bullet.GetComponent<Bullet>());
        bullet = Instantiate(bulletPrefab, collisionPlace, Quaternion.identity, GetBulletParent());
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -INIT_BULLET_SPEED);
        elements.Add(bullet.GetComponent<Bullet>());
    }

    //Spawning enemy bullets that are directed at the player.
    public void ShootBulletAtPlayer(Vector3 from, float speed) {
        Vector3 direction = (player.transform.position - from).normalized;
        Quaternion rotation = PhysicUtils.LookRotation2D(direction);
        GameObject enemyBullet = Instantiate(enemyBulletPrefab, from, rotation, GetBulletParent());
        enemyBullet.GetComponent<Rigidbody2D>().velocity = direction * speed;
        elements.Add(enemyBullet.GetComponent<Bullet>());
    }


    public static Transform GetBulletParent() {
        return GameObject.FindGameObjectWithTag("BulletSpawn").transform;
    }
}
