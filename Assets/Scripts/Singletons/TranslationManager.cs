﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;

[Serializable]
public class LocalizationData {
    public List<LocalizationItem> data;
}

[Serializable]
public class LocalizationItem {
    public string key;
    public string value;
}

[Serializable]
public class IndexData {
    public List<IndexItem> IndexEntries;
    public int version;
}

[Serializable]
public class IndexItem {
    public string IndexEntry;
}

// Class responsible for handling translations.

public class Translations : Singleton<Translations> {

    protected Translations() {

    }

    //Default language if we don't know player prefs, or player prefs are invalid.
    public const string DEFAULT_LANGUAGE = "English";

    private Dictionary<string, Dictionary<string, string>> localizedTexts = new Dictionary<string, Dictionary<string, string>>();


    int toLoad = 1;
    int loaded = 0;
    private string missingTextString = "Localized text not found";

    //List of available IndexEntries
    public List<string> languages = new List<string>();
    //Index on which is current language (from player prefs).
    public int CurrentLangIndex = -1;

    public bool IsLoaded() {
        return toLoad == loaded;
    }

    void AddTranslation(LocalizationData loadedData, string language) {
        string loadedLang = PlayerPrefs.GetString("Language", DEFAULT_LANGUAGE);
        Dictionary<string, string> translations = new Dictionary<string, string>();
        for (int j = 0; j < loadedData.data.Count; j++) {
            translations[loadedData.data[j].key] = loadedData.data[j].value;
        }
        localizedTexts[language] = translations;
        loaded++;
        languages.Add(language);
        if (loadedLang == language) {
            CurrentLangIndex = languages.Count - 1;
        }
    }

    void InitLanguage(string language, FileHandler fh) {
        StartCoroutine(fh.FileExists(language + ".json",
            (langExists) => {
                if (!langExists) {
                    Debug.LogError("Language " + language + " file does not exist!");
                    toLoad--;
                    return;
                }
                StartCoroutine(fh.GetFile(language + ".json",
                    (langText) => {
                        LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(langText);
                        AddTranslation(loadedData, language);
                    }));
            }));
    }

    void InitAllLanguages(IndexData indexData, FileHandler fh) {
        string loadedLang = PlayerPrefs.GetString("Language", DEFAULT_LANGUAGE);
        if (!indexData.IndexEntries.Exists((l) => l.IndexEntry == loadedLang)) {
            Debug.LogError("Playerpref language " + loadedLang + " does not exist. Using default instead!");
            PlayerPrefs.SetString("Language", DEFAULT_LANGUAGE);
        }
        toLoad = indexData.IndexEntries.Count;
        for (int i = 0; i < indexData.IndexEntries.Count; i++) {
            string language = indexData.IndexEntries[i].IndexEntry;
            InitLanguage(language, fh);
        }
    }

    public void Init() {
        FileHandler fh = FileHandler.GetFileHandler(Path.Combine(Application.streamingAssetsPath, "Translations"));
        StartCoroutine(fh.FileExists("index.json", 
            (exists) => {
                if (!exists) {
                    throw new Exception("Translation index not found");
                }
                StartCoroutine(fh.GetFile("index.json", 
                    (index) => {
                        IndexData indexData = JsonUtility.FromJson<IndexData>(index);
                        InitAllLanguages(indexData, fh);
                    }
                ));
            }
        ));
    }


    public string GetLocalizedValue(string key, List<string> parameters) {
        string currentLanguage = PlayerPrefs.GetString("Language", DEFAULT_LANGUAGE);
        string result = missingTextString;
        if (localizedTexts[currentLanguage].ContainsKey(key)) {
            result = localizedTexts[currentLanguage][key];
        }
        for (int i = 0; i < parameters.Count; i++) {
            result = new Regex("\\{" + i + "}").Replace(result, parameters[i]);
        }
        return result;

    }
}
