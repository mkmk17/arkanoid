﻿using UnityEngine;
using UnityEngine.SceneManagement;


//Class responsible for managing gameplay. Keeping score and life meter. Whether it is won, paused, lost. Cleaning level, managing other managers.
public class GamePlay : WeakSingleton<GamePlay> {
    protected GamePlay() {
    }

    //If it's true than player didn't shot any ball yet. Enemies should not shoot during this time, etc.
    public bool NotStartedYet {
        get; set;
    }

    public bool GameOver {
        get; private set;
    }
    public bool LevelWon {
        get; private set;
    }

    public int Score {
        get {
            return SetTracker.Instance.Score;
        }
        set {
            SetTracker.Instance.Score = value;
        }
    }
    public int Life {
        get {
            return SetTracker.Instance.Life;
        }
        set {
            SetTracker.Instance.Life = value;
        }
    }


    void Start() {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
        NotStartedYet = true;
        SoundPlayer.Instance.StartMusic();
        InitializeGameValues();
        CreateBlocks();
    }

    void InitializeGameValues() {
        Score = SetTracker.Instance.Score;
        Life = SetTracker.Instance.Life;
    }

    void CreateBlocks() {
        LevelCreator.Instance.CreateLevel();
    }


    public void WinGame() {
        CleanLevel();
        LevelWon = true;
        GameOver = true;
        if (LevelCreator.Instance.IsLastLevel()) {
            SaveUtils.MarkSetAsFinished(SetTracker.Instance.CurrentLevel.Set);
        }
        DoPause();
    }

    public void LostLife() {
        NotStartedYet = true;
        CleanLevel();
        SoundPlayer.Instance.PlayLifeLoss();
        Life--;
        Score -= ScoringConstants.LOST_LIFE_PENALTY;
        if (Life <= 0) {
            GameOver = true;
            DoPause();
        }
        else {
            BallManager.Instance.SpawnBall();
        }
    }

    void CleanLevel() {
        BallManager.Instance.ClearLevel();
        BonusManager.Instance.ClearLevel();
        BlockManager.Instance.ClearLevel();
        BulletManager.Instance.ClearLevel();
        ExplosionManager.Instance.ClearLevel();        
    }

    public void NextLevelButtonPressed() {
        if (!GameOver || !LevelWon || LevelCreator.Instance.IsLastLevel())
            return;
        SetTracker.Instance.CurrentLevel = SetTracker.Instance.CurrentLevel.NextLevel();
        SceneManager.LoadScene("Gameplay");
    }

    public bool IsPaused = false;
    public void Pause() {
        if (GameOver) {
            return;
        }
        if (IsPaused) {
            DoUnpause();
        }
        else {
            DoPause();
        }
    }

    void DoPause() {
        IsPaused = true;
        foreach (GameObject block in GameObject.FindGameObjectsWithTag("Block")) {
            var pausable = block.GetComponent<PatternMovingBlock>();
            if (pausable != null) {
                pausable.Pause();
            }
        }
        GamePlayUI.Instance.Pause();
        BallManager.Instance.Pause();
        BonusManager.Instance.Pause();
        BulletManager.Instance.Pause();
        ExplosionManager.Instance.Pause();
    }

    void DoUnpause() {
        IsPaused = false;
        foreach (GameObject block in GameObject.FindGameObjectsWithTag("Block")) {
            var pausable = block.GetComponent<PatternMovingBlock>();
            if (pausable != null) {
                pausable.UnPause();
            }
        }
        GamePlayUI.Instance.UnPause();
        BallManager.Instance.UnPause();
        BonusManager.Instance.UnPause();
        BulletManager.Instance.UnPause();
        ExplosionManager.Instance.UnPause();
    }

    public void QuitGame() {
        Game.Instance.EndGamePlay();
    }

    public void ResetGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
