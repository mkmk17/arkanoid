﻿//Bonus that makes all currently existing balls to move slower for DURATION seconds.
public class SlowBallBonus : Bonus {
    private const int DURATION = 15;

    protected override void Start() {
        base.Start();
        pointsForBonus = ScoringConstants.SLOW_BALL_POINTS;
    }

    protected override void DoActivate() {
        BallManager.Instance.BallMap(ball => ball.SlowDown());
        TimedBonus revert = new TimedBonus(GameTime.Instance.TotalTime + DURATION,
            () => {
                BallManager.Instance.BallMap(ball => ball.NormalSpeed());
            }, TimedBonusType.BallSpeed);
        BonusManager.Instance.AddTimeBonus(revert);
        DestroyInstance();
    }
}
