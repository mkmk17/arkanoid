﻿//Bonus that makes all currently existing balls to move faster for DURATION seconds.
public class FastBallBonus : Bonus {
    private const int DURATION = 15;

    protected override void Start() {
        base.Start();
        pointsForBonus = ScoringConstants.FAST_BALL_POINTS;
    }

    protected override void DoActivate() {
        BallManager.Instance.BallMap(ball => ball.SpeedUp());
        TimedBonus revert = new TimedBonus(GameTime.Instance.TotalTime + DURATION,
            () => {
                BallManager.Instance.BallMap(ball => ball.NormalSpeed());
            }, TimedBonusType.BallSpeed);
        BonusManager.Instance.AddTimeBonus(revert);
        DestroyInstance();
    }
}
