﻿//Bonus that makes ball explode when it hits a block
public class ExplodingBallBonus : Bonus {
    private const int DURATION = 15;

    protected override void Start() {
        base.Start();
        pointsForBonus = ScoringConstants.FAST_BALL_POINTS;
    }

    protected override void DoActivate() {
        BallManager.Instance.BallMap(ball => ball.IsExploding = true);
        TimedBonus revert = new TimedBonus(GameTime.Instance.TotalTime + DURATION,
            () => {
                BallManager.Instance.BallMap(ball => ball.IsExploding = false);
            }, TimedBonusType.ExplodingBall);
        BonusManager.Instance.AddTimeBonus(revert);
        DestroyInstance();
    }
}
