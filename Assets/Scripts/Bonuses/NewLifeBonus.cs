﻿//"Bonus" that adds +1 life
public class NewLifeBonus : Bonus {

    protected override void Start() {
        base.Start();
        pointsForBonus = ScoringConstants.NEW_LIFE_POINTS;
    }

    protected override void DoActivate() {
        GamePlay.Instance.Life++;
        DestroyInstance();
    }

}
