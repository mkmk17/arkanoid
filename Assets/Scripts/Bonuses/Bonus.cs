﻿using UnityEngine;
using System;

//Type of timed bonus. If there is currently active bonus of same type then another bonus cancel it.
public enum TimedBonusType {
    StrongBall = 0,
    BallSpeed = 1,
    ExplodingBall = 2,
}

//Class responsible for storing action that reverts timed bonus.
public class TimedBonus {
    public TimedBonusType TimedBonusType;
    //When action should be done.
    public int EndTime;
    //Action reverting timed bonus.
    public Action ActionToDo;

    public TimedBonus(int endTime, Action actionToDo, TimedBonusType timedBonusType) {
        EndTime = endTime;
        ActionToDo = actionToDo;
        TimedBonusType = timedBonusType;
    }
}

//Base class for all bonuses.
[RequireComponent(typeof (Rigidbody2D), typeof(Collider2D))]
public abstract class Bonus : PausableBehaviour, ISingleInstance {
    const float SPEED = 16;
    protected bool bonusSound;
    protected int pointsForBonus;

    protected override void Start() {
        transform.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -SPEED);
        bonusSound = true;
        pointsForBonus = 0;
        base.Start();
    }

    protected float AudioLength() {
        return SoundPlayer.Instance.BonusSoundLength();
    }

    protected abstract void DoActivate();

    public void Activate() {
        if (isDestroyed) return;
        GamePlay.Instance.Score += pointsForBonus;
        if (bonusSound) {
            SoundPlayer.Instance.PlayBonusSound();
        }
        DoActivate();
    }

    public void DestroyInstance() {
        BonusManager.Instance.DestroyBonus(this);
    }
}
