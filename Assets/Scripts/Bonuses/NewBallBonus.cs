﻿using UnityEngine;

//Bonus that spawns another ball when it hits the paddle.
public class NewBallBonus : Bonus {

    protected override void Start() {
        base.Start();
        pointsForBonus = ScoringConstants.NEW_BALL_POINTS;
    }

    protected override void DoActivate() {
        GameObject ball = GameObject.FindGameObjectWithTag("Ball");
        if (ball != null) {
            BallManager.Instance.SpawnBall(ball.transform.position);
        }
        DestroyInstance();
    }

}
