﻿//"Bonus" that makes player lost life if it hits paddle.

public class HarmfulBonus : Bonus
{
    protected override void Start()
    {
        base.Start();
        bonusSound = false;
    }

    protected override void DoActivate()
    {
        GamePlay.Instance.LostLife();
    }
}