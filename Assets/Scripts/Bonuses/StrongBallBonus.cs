﻿//Bonus that makes ball go through blocks.
public class StrongBallBonus : Bonus {
    private const int DURATION = 15;

    protected override void Start() {
        base.Start();
        pointsForBonus = ScoringConstants.STRONG_BALL_POINTS;
    }

    protected override void DoActivate() {
        BallManager.Instance.BallMap(ball => ball.IsStrong = true);
        TimedBonus revert = new TimedBonus(GameTime.Instance.TotalTime + DURATION,
            () => {
                BallManager.Instance.BallMap(ball => ball.IsStrong = false);
            }, TimedBonusType.StrongBall);
        BonusManager.Instance.AddTimeBonus(revert);
        DestroyInstance();
    }
}
