﻿using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleExplosion : BaseExplosion {
    public override void DestroyInstance() {
        ExplosionManager.Instance.RemoveElement(this);
    }

    public Color Color;
    private ParticleSystem p;

    protected override void Start () {
        p = GetComponent<ParticleSystem>();
        var settings = p.main;
        settings.startColor = Color;
        p.Play();
        base.Start();
    }

    private void Update() {
        if (!p.IsAlive()) {
            DestroyInstance();
        }
    }

    public override void Pause() {
        base.Pause();
        if (p != null && p.isPlaying) {
            p.Pause();
        }
    }

    public override void UnPause() {
        base.UnPause();
        if (p != null && p.isPaused) {
            p.Play();
        }
    }
}
