﻿using UnityEngine;

//Script for left,right and up boundary of game.
[RequireComponent ( typeof(Collider2D) )]
public class GameBoundary : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Bullet" || collision.gameObject.tag == "EnemyBullet") {
            collision.GetComponent<ISingleInstance>().DestroyInstance();
        }
    }
}
