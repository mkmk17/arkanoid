﻿using UnityEngine;

//Class responsible for handling collisions on down border of the level.
[RequireComponent(typeof(Collider2D))]
public class LowerBound : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Ball" || other.gameObject.tag == "Bonus" || other.gameObject.tag == "Bullet" || other.gameObject.tag == "EnemyBullet") {
            other.gameObject.GetComponent<ISingleInstance>().DestroyInstance();
        }
    }
}
