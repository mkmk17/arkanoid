﻿public class Bullet : PausableBehaviour, ISingleInstance {

    public void DestroyInstance() {
        BulletManager.Instance.RemoveElement(this);
    }
}
