﻿using UnityEngine;

//Class responsible for handling paddle collisions.
[RequireComponent(typeof(Collider2D))]
public class PaddleColider : MonoBehaviour {

    //Maximum angle of direction of the ball that hit the paddle.
    const float MAX_ANGLE = 75;
    
    //Padle radius
    const float RADIUS = 7;

    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Ball") {
            Rigidbody2D ballRb = other.gameObject.GetComponent<Rigidbody2D>();
            float collisionPoint = transform.position.x - other.transform.position.x;
            Vector2 direction = Quaternion.Euler(0, 0, PhysicUtils.MapValue(collisionPoint, -RADIUS, RADIUS, -MAX_ANGLE, MAX_ANGLE)) * Vector2.up;
            ballRb.velocity = direction * other.gameObject.GetComponent<Ball>().GetSpeed();
            SoundPlayer.Instance.PlayPaddleSound();
        }
        else if (other.gameObject.tag == "Bonus") {
            other.gameObject.GetComponent<Bonus>().Activate();
        } else if (other.gameObject.tag == "EnemyBullet") {
            GamePlay.Instance.LostLife();
        }
    }
}
