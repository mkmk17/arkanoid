﻿using UnityEngine;

//Class responsible for handling player input.
public class PlayerControl : MonoBehaviour {

    //Vertical position of the paddle. In doesn't change through the whole game.
    float y;
    bool mouseControls;
    Rigidbody2D rb;
    private int direction;

    void Start() {
        y = transform.position.y;
        mouseControls = PlayerPrefs.GetInt("Controls", 1) == 1;
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    void Update() {
        if (DebugUtils.HasFlag(DebugUtils.level, DebugLevel.GamePlayDebug)) {
            if (Input.GetKeyDown(KeyCode.R)) {
                GamePlay.Instance.ResetGame();
            }
            if (Input.GetKeyDown(KeyCode.W)) {
                GamePlay.Instance.WinGame();
            }
            if (Input.GetKeyDown(KeyCode.Z)) {
                string path = @"C:\img\UnityScreenshots\Screenshot" + Random.Range(0, 100) + ".png";
                ScreenCapture.CaptureScreenshot(path);
                Debug.Log("Captured to " + path);
            }
        }
        if (Input.GetKeyDown(KeyCode.P)) {
            GamePlay.Instance.Pause();
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            GamePlay.Instance.QuitGame();
        }
        if (
            (mouseControls && Input.GetMouseButtonDown(0)) ||
            (!mouseControls && Input.GetKeyDown(KeyCode.Space))
           ) {
            if (GamePlay.Instance.GameOver) {
                GamePlay.Instance.NextLevelButtonPressed();
            }
            else if (!GamePlay.Instance.IsPaused) {
                TryStartingBall();
            }
        }
        if (GamePlay.Instance.GameOver || GamePlay.Instance.IsPaused)
        {
            rb.velocity = Vector2.zero;
            return;
        }
        UpdatePaddle();

    }

    void UpdatePaddle() {
        if (mouseControls) {
            Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pz.z = 0;
            pz.y = y;
            pz.x = Mathf.Clamp(pz.x, -60, 60);
            rb.MovePosition(pz);
        }
        else {
            if (Input.GetKey(KeyCode.LeftArrow)) {
                if (direction > 0) {
                    rb.velocity = Vector2.zero;
                }
                rb.AddForce(new Vector2(-Time.deltaTime, 0), ForceMode2D.Force);
                direction = -1;
            }
            else if (Input.GetKey(KeyCode.RightArrow)) {
                if (direction < 0) {
                    rb.velocity = Vector2.zero;
                }
                rb.AddForce(new Vector2(Time.deltaTime, 0), ForceMode2D.Force);
                direction = 1;
            }
            else
            {
                rb.velocity = Vector2.zero;
                direction = 0;
            }
            Vector3 pz = transform.position;
            if (Mathf.Abs(pz.x) > 60) {
                pz.x = Mathf.Clamp(pz.x, -60, 60);
                rb.MovePosition(pz);
            }
        }
    }

    void TryStartingBall() {
        if (GamePlay.Instance.NotStartedYet) {
            GamePlay.Instance.NotStartedYet = false;
            foreach (GameObject ball in GameObject.FindGameObjectsWithTag("Ball")) {
                ball.GetComponent<Ball>().TryStartBall();
            }
        }
    }


}
