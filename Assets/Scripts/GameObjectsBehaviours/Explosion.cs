﻿using UnityEngine;

//Class for explosion that is created by exploding ball (and maybe by exploding blocks in future
public class Explosion : BaseExplosion {
    public override void DestroyInstance() {
        ExplosionManager.Instance.RemoveElement(this);
    }

    private ParticleSystem p;

    protected override void Start () {
        p = GetComponent<ParticleSystem>();
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 6);
        foreach (Collider2D collider in colliders) {
            if (collider.gameObject.tag == "Block" && collider.gameObject.GetComponent<Block>().CanBeDestroyed()) {
                collider.gameObject.GetComponent<Block>().ScoreAndDestroy();
            }
        }
        base.Start();
	}

    private void Update() {
        if (!p.IsAlive()) {
            DestroyInstance();
        }
    }

    public override void Pause() {
        base.Pause();
        if (p.isPlaying) {
            p.Pause();
        }
    }

    public override void UnPause() {
        base.UnPause();
        if (p.isPaused) {
            p.Play();
        }
    }
}
