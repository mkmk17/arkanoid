﻿public abstract class BaseExplosion : PausableBehaviour, ISingleInstance {
    public abstract void DestroyInstance();
}
