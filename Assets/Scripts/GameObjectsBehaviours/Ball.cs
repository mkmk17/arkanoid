﻿using UnityEngine;

public enum BallState {
    Normal,
    Slowed,
    Faster
}

//Class responsible for single instance of ball.
[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class Ball : PausableBehaviour, ISingleInstance {

    Rigidbody2D rigidBody;

    //Initial speed magnitude.
    private const float INIT_MAGNITUDE = 70;
    private const float FAST_MAGNITUDE = 100;
    private const float SLOW_MAGNITUDE = 40;

    public float GetSpeed() {
        switch (state) {
            case BallState.Slowed:
                return SLOW_MAGNITUDE;
            case BallState.Faster:
                return FAST_MAGNITUDE;
            default:
                return INIT_MAGNITUDE;
        }
    }
    public BallState state;

    //If Started is true it means that ball is stuck to the paddle, and is waiting to be shot by player.
    public bool Started;

    private void UpdateColor() {
        Color? speedColor = null;
        switch (state) {
            case BallState.Faster:
                speedColor = Color.red;
                break;
            case BallState.Slowed:
                speedColor = Color.blue;
                break;
            default:
                speedColor = null;
                break;
        }

        if (isStrong) {
            if (speedColor.HasValue) {
                GetComponent<SpriteRenderer>().color = Color.Lerp(speedColor.Value, Color.green, 0.5f);
            }
            else {
                GetComponent<SpriteRenderer>().color = Color.green;
            }
        }
        else {
            if (speedColor.HasValue) {
                GetComponent<SpriteRenderer>().color = speedColor.Value;
            }
            else {
                GetComponent<SpriteRenderer>().color = Color.yellow;
            }
        }
       
    }

    public bool IsStrong {
        get {
            return isStrong;
        }
        set {
            isStrong = value;
            UpdateColor();
            gameObject.layer = value ? 13 : 9;
        }
    }
    private bool isStrong = false;

    public bool IsExploding {
        get {
            return isExploding;
        }
        set {
            isExploding = value;
            transform.GetChild(0).gameObject.SetActive(value);
        }
    }
    private bool isExploding = false;

    //Is not null when ball was just teleported by teleport block. It is removed when it leaves collider of destination teleport block.
    public GameObject TeleportedBy;

    protected override void Start() {
        TeleportedBy = null;
        state = BallState.Normal;
        rigidBody = GetComponent<Rigidbody2D>();
        transform.GetChild(0).gameObject.SetActive(false);
        if (!Started)
            return;
        Vector2 dir = Quaternion.Euler(0, 0, Random.Range(-180, 180)) * Vector2.up;
        rigidBody.velocity = dir * INIT_MAGNITUDE;
        base.Start();
    }

    private void FixedUpdate() {
        if (!Started) {
            rigidBody.MovePosition(BallManager.Instance.PaddleSpawnPoint.transform.position);
        }
        //If ball somehow went out of bound then it should be destroyed.
        if (Mathf.Abs(transform.position.y) > 80 || Mathf.Abs(transform.position.x) > 80)
        {
            if (DebugUtils.HasFlag(DebugUtils.level, DebugLevel.DebugLogs)) {
                Debug.LogWarning("DESTROYED BALL OUT OF BOUNDS x = " + transform.position.x + " y = " + transform.position.y);
            }
            BallManager.Instance.RemoveElement(this);
        }
    }

    //If ball is stuck to the paddle then add force into the paddle to start it.
    public void TryStartBall() {
        if (Started)
            return;
        rigidBody.velocity = Vector2.up * INIT_MAGNITUDE;
        transform.parent = BallManager.Instance.GetBallParent();
        Started = true;
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Ball") {
            if (transform.gameObject.GetInstanceID() < collision.gameObject.GetInstanceID())
                return;
            BulletManager.Instance.SpawnBulletsAfterBallsCollision(transform.position);
        }
        else if (isExploding && collision.gameObject.tag == "Block" && !(collision.gameObject.GetComponent<Block>() is TeleportBlock)) {
            ExplosionManager.Instance.CreateExplosion(transform.position);
        }
    }

    //Collision may change speed.
    void KeepSpeedConstant() {
        float currentMagnitude;
        switch (state) {
            case BallState.Faster: 
                currentMagnitude = FAST_MAGNITUDE;
                break;
            case BallState.Slowed:
                currentMagnitude = SLOW_MAGNITUDE;
                break;
            default:
                currentMagnitude = INIT_MAGNITUDE;
                break;
        }
        rigidBody.velocity = rigidBody.velocity.normalized * currentMagnitude;
    }

    private void LateUpdate() {
        KeepSpeedConstant();
    }

    public void SpeedUp() {
        state = BallState.Faster;
        UpdateColor();
    }

    public void NormalSpeed() {
        state = BallState.Normal;
        UpdateColor();
    }

    public void SlowDown() {
        state = BallState.Slowed;
        UpdateColor();
    }

    public void DestroyInstance() {
        BallManager.Instance.DestroyBall(this);
    }
}
