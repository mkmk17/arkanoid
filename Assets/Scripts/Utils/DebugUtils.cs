﻿using System;
using UnityEngine;

//What level of debugging is enabled.
// PamePlayDebug - gameplay is changed: Pressing W wins level. Pressing R resets level. Bonuses rate is 0.6
// DebugLogs - some useful logs are displayed. 
[Flags]
public enum DebugLevel {
    None = 0,
    GamePlayDebug = 1,
    DebugLogs = 2,
}

//Class for various utils connected to debbuging.
public static class DebugUtils {

    public static bool HasFlag(DebugLevel val, DebugLevel flag) {
        return (val & flag) == flag;
    }

    public static DebugLevel level = Application.isEditor ? DebugLevel.GamePlayDebug : DebugLevel.None;
}
