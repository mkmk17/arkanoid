﻿//Class for handling some physics.
using UnityEngine;

public static class PhysicUtils {

    //http://gamedev.stackexchange.com/questions/1036/what-are-your-favourite-game-specific-coding-gems
    public static float MapValue(float inVal, float inFrom, float inTo, float outFrom, float outTo) {
        float inScale = (inFrom != inTo)
            ? ((inVal - inFrom) / (inTo - inFrom))
            : 0.0f;
        float outVal = outFrom + (inScale * (outTo - outFrom));
        outVal = (outFrom < outTo)
            ? Mathf.Clamp(outVal, outFrom, outTo)
            : Mathf.Clamp(outVal, outTo, outFrom);
        return outVal;
    }

    //http://answers.unity3d.com/questions/585035/lookat-2d-equivalent-.html
    public static Quaternion LookRotation2D(Vector3 dir) {
        float rot_z = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        return Quaternion.Euler(0f, 0f, rot_z - 90);
    }
}
