﻿using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.IO;
using System;

//Data structure used to keep save data.
[Serializable]
class SaveData {
    public Dictionary<string, int> Sets;
}


//Class responsible for saving and loading the high scores / completed sets.
public class SaveUtils : MonoBehaviour {

    private static readonly string SAVE_FILE_PATH = Application.persistentDataPath + "/arkanoid.sav";

    public static void MarkSetAsFinished(string set) {
        Dictionary<string, int> scores = GetSetsScores();
        SaveData data = new SaveData();
        if (!scores.ContainsKey(set) || scores[set] < SetTracker.Instance.RealScore) {
            scores[set] = SetTracker.Instance.RealScore;
        }   
        data.Sets = scores;
        FileStream file = File.Open(SAVE_FILE_PATH, FileMode.Create);
        new BinaryFormatter().Serialize(file, data);
        file.Close();
    }

    public static Dictionary<string, int> GetSetsScores() {
        BinaryFormatter bf = new BinaryFormatter();
        SaveData data = null;
        FileStream file = null;
        if (File.Exists(SAVE_FILE_PATH)) {
            file = File.Open(SAVE_FILE_PATH, FileMode.Open);
            data = (SaveData)bf.Deserialize(file);
        }
        else {
            return new Dictionary<string, int>();
        }
        if (data == null) {
            data = new SaveData();
        }
        if (data.Sets == null) {
            data.Sets = new Dictionary<string, int>();
        }
        file.Close();
        return data.Sets;
    }

    public static void ClearSave() {
        if (File.Exists(Application.persistentDataPath + "/arkanoid.sav")) {
            File.Delete(Application.persistentDataPath + "/arkanoid.sav");
        }
    }
}
