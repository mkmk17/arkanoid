﻿using System;

[Serializable]
public class Pair<T1, T2> {
    public T1 First {
        get; private set;
    }
    public T2 Second {
        get; private set;
    }

    public Pair(T1 first, T2 second) {
        First = first;
        Second = second;
    }

    public Pair()
    {
    }
}

[Serializable]
public class Pair<T1> {
    public T1 First {
        get; private set;
    }
    public T1 Second {
        get; private set;
    }
    public Pair(T1 first, T1 second) {
        First = first;
        Second = second;
    }

    public Pair() {
    }
}