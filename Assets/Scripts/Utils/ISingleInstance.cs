﻿//Interface for classes that can have multiple instances and are managed by managers.
public interface ISingleInstance {

    void DestroyInstance();
}
