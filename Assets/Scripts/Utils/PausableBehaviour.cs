﻿using UnityEngine;

//Class for behaviour that should be pausable.
public class PausableBehaviour : MonoBehaviour {
    Vector2 velocity;
    float angularVelocity;
    protected bool paused;
    protected bool isDestroyed;

    protected virtual void Start() {
        if (GamePlay.Instance.IsPaused) {
            Pause();
        }
    }

    private void PauseRigidBody() {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        if (rb == null) {
            return;
        }
        velocity = rb.velocity;
        angularVelocity = rb.angularVelocity;
        rb.Sleep();
    }

    private void UnPauseRigidBody() {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        if (rb == null) {
            return;
        }
        rb.WakeUp();
        rb.velocity = velocity;
        rb.angularVelocity = angularVelocity;
    }

    private void PauseAnimator() {
        Animator animator = GetComponent<Animator>();
        if (animator == null) {
            return;
        }
        animator.enabled = false;
    }

    private void UnPauseAnimator() {
        Animator animator = GetComponent<Animator>();
        if (animator == null) {
            return;
        }
        animator.enabled = true;
    }

    public virtual void Pause() {
        paused = true;
        PauseRigidBody();
        PauseAnimator();
    }

    public virtual void UnPause() {
        paused = false;
        UnPauseRigidBody();
        UnPauseAnimator();
    }

    public virtual void DestroyMe()
    {
        isDestroyed = true;
        Destroy(gameObject);
    }

    protected void OnDisable()
    {
        if (isDestroyed) return;
        DestroyMe();
    }
}