﻿using UnityEngine;

//Class that contains information which level is currently played.
public class LevelInfo {

    public string Set {
        get; private set;
    }

    public int LevelNo {
        get; private set;
    }

    public Vector2 BallSpawnPoint {
        get; private set;
    }

    public LevelInfo(string set, int levelNo = 1) {
        Set = set;
        LevelNo = levelNo;
        if (Set == "A" && LevelNo == 2) {
            BallSpawnPoint = new Vector2(0, -10);
        }
        else {
            BallSpawnPoint = Vector2.zero;
        }
    }

    public LevelInfo NextLevel() {
        return new LevelInfo(Set, LevelNo + 1);
    }
}
