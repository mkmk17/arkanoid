﻿public static class ScoringConstants {
    public const int BLOCK_POINTS = 100;
    public const int NEW_LIFE_POINTS = 500;
    public const int NEW_BALL_POINTS = 200;
    public const int FAST_BALL_POINTS = 200;
    public const int SLOW_BALL_POINTS = 200;
    public const int STRONG_BALL_POINTS = 200;
    public const int LOST_LIFE_PENALTY = 1000;
}
