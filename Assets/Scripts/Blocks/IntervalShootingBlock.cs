﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

//Params for initializator.
[Serializable]
public class ShootingIntervalParams {
    public int Interval;
    public int Offset;
    public float BulletSpeed;
    public Pair<int> IntervalRandomness;
    public Pair<int> OffsetRandomness;
    public bool NotNull;

    public ShootingIntervalParams(int interval = 4, float bulletSpeed = 18.0f, int offset = 0, Pair<int> intervalRandomness = null, Pair<int> offsetRandomness = null) {
        Interval = interval;
        Offset = offset;
        BulletSpeed = bulletSpeed;
        IntervalRandomness = intervalRandomness;
        OffsetRandomness = offsetRandomness;
        NotNull = true;
    }
}

//Behavoiur of shooting bullets at the player at interval.
public class IntervalShootingBlock : PausableBehaviour {
    //Time of last shot
    private int lastShot;
    //Interval in seconds
    private int interval;
    //Offset in seconds, so shooting enemies with same interval don't have to shoot always at the same time.
    private int offset;
    private float bulletSpeed;

    public void Init(ShootingIntervalParams shootParams) {
        gameObject.GetComponent<Block>().ParticleColor = Color.magenta;
        offset = shootParams.OffsetRandomness == null ?
            shootParams.Offset :
            shootParams.Offset + Random.Range(shootParams.OffsetRandomness.First, shootParams.OffsetRandomness.Second);
        interval = shootParams.IntervalRandomness == null ?
            shootParams.Interval :
            shootParams.Interval + Random.Range(shootParams.IntervalRandomness.First, shootParams.IntervalRandomness.Second);
        bulletSpeed = shootParams.BulletSpeed;
        lastShot = GameTime.Instance.TotalTime + offset;
    }
	
	void Update () {
        if (ShouldItShoot()) {
            lastShot = GameTime.Instance.TotalTime;
            if (paused || GamePlay.Instance.NotStartedYet) {
                return;
            }
            ShootBullet();
        }
	}

    bool ShouldItShoot() {
        return GameTime.Instance.TotalTime - lastShot >= interval;
    }

    void ShootBullet() {
        BulletManager.Instance.ShootBulletAtPlayer(transform.position, bulletSpeed);
    }
}
