﻿using UnityEngine;

//Base class for blocks.
[RequireComponent(typeof (SpriteRenderer), typeof (Collider2D))]
public abstract class Block : PausableBehaviour, ISingleInstance {

    public Color BlockColor;
    public Color? ParticleColor;

    protected override void Start() {
        GetComponent<SpriteRenderer>().color = BlockColor;
        base.Start();
    }

    //Some blocks when destroyed can drop a bonus.
    protected void DropBonus() {
        BonusManager.Instance.DropBonus(transform.position);
    }

    public void DestroyInstance()
    {
        if (isDestroyed) return;
        BlockManager.Instance.DestroyBlock(this);
    }

    //Can be destroyed by player.
    public virtual bool CanBeDestroyed() {
        return false;
    }

    //Destruction by player. Score is added, block count is updated.
    public virtual void ScoreAndDestroy() {
        ExplosionManager.Instance.BlockDestruction(transform.position, ParticleColor.HasValue ? ParticleColor.Value : BlockColor);
        DestroyInstance();        
    }
}
