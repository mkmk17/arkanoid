﻿using UnityEngine;

//Block that is linked with another teleport block and teleports ball when touched by ball.
public class TeleportBlock : Block {

    public GameObject LinkedTeleport;

    override protected void Start() {
        BlockColor = Color.white;
        base.Start();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Ball") {
            if (collision.gameObject.GetComponent<Ball>().TeleportedBy == LinkedTeleport) {
                collision.gameObject.GetComponent<Ball>().TeleportedBy = null;
                return;
            }
            SoundPlayer.Instance.PlayTeleportSound();
            collision.gameObject.transform.position = LinkedTeleport.transform.position;
            collision.gameObject.GetComponent<Ball>().TeleportedBy = gameObject;
        }
    }
}
