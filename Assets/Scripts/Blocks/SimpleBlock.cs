﻿using UnityEngine;

//Simple block.
public class SimpleBlock : Block {

    private float BONUS_RATE;
    public int MaxHP;
    public int CurrentHP;

    //What is next state of the block after being damaged. Null means that it is destroyed.
    int? NextState() {
        switch (MaxHP) {
            case 1:
                return null;
            case 2:
                switch (CurrentHP) {
                    case 2:
                        return 2;
                    default:
                        return null;
                }
            case 3:
                switch (CurrentHP) {
                    case 3:
                        return 2;
                    case 2:
                        return 3;
                    default:
                        return null;
                }
            case 4:
                switch (CurrentHP) {
                    case 4:
                        return 1;
                    case 3:
                        return 2;
                    case 2:
                        return 3;
                    default:
                        return null;
                }
            default:
                throw new System.Exception("Not implemented sprites for maxhp for block = " + MaxHP);
        }
    }

    //Sometimes block was hit by multiple balls before it was destroyed which lead to exceptions.
    // OnCollisionEnter2D should be done only once in lifetime of block;
    private bool wasCalledAlready;

    protected override void Start() {
        base.Start();
        CurrentHP = MaxHP;
        BONUS_RATE = DebugUtils.HasFlag(DebugUtils.level, DebugLevel.GamePlayDebug) ? 0.6f : 0.15f;
        wasCalledAlready = false;
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (wasCalledAlready)
            return;
        if (collision.gameObject.tag == "Ball") {
            DamageBlock();
        }
    }

    
    private void OnTriggerEnter2D(Collider2D collision) {
        if (wasCalledAlready)
            return;
        if (collision.gameObject.tag == "Bullet") {
            collision.gameObject.GetComponent<ISingleInstance>().DestroyInstance();
            DamageBlock();
        }
        else if (collision.gameObject.tag == "Ball" && collision.gameObject.GetComponent<Ball>().IsStrong) {
            DamageBlock(true);
            if (collision.gameObject.GetComponent<Ball>().IsExploding) {
                ExplosionManager.Instance.CreateExplosion(collision.gameObject.transform.position);
            }
        }
    }

    private void DamageBlock(bool alwaysDestroy = false) {
        wasCalledAlready = true;
        SoundPlayer.Instance.PlayBlockSound();
        int? nextState = NextState();
        if (nextState.HasValue && !alwaysDestroy) {
            //Block is only damaged
            CurrentHP--;
            GetComponent<SpriteRenderer>().sprite = BlockManager.Instance.SimpleBlockSprite(nextState.Value, GetComponent<IntervalShootingBlock>() == null);
            ExplosionManager.Instance.CreateLightDamage(transform.position, ParticleColor.HasValue ? ParticleColor.Value : BlockColor);
        }
        else {
            //Block is destroyed
            ScoreAndDestroy();
        }
        wasCalledAlready = false;
    }

    public override bool CanBeDestroyed() {
        return true;
    }

    public override void ScoreAndDestroy() {
        if (Random.value < BONUS_RATE) {
            DropBonus();
        }
        GamePlay.Instance.Score += ScoringConstants.BLOCK_POINTS;
        base.ScoreAndDestroy();
    }
}
