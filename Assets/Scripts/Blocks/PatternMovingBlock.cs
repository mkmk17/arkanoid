﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Behaviour of moving rigidbody2D in repeatable pattern.
[RequireComponent(typeof (Rigidbody2D))]
public class PatternMovingBlock : PausableBehaviour {
    //List of possition though which object is moving in cycle.
    public List<Vector2> pattern;
    //Where are we currently going to.
    private int currentIndex;
    public float speed;
    private Rigidbody2D rb;

    protected override void Start() {
        rb = GetComponent<Rigidbody2D>();
        transform.position = pattern[0];
        IEnumerator courutine = MoveFromTo();
        StartCoroutine(courutine);
        base.Start();
    }

    private IEnumerator MoveFromTo() {
        while (true) {
            Vector2 a = transform.position;
            Vector2 b = pattern[(currentIndex + 1) % pattern.Count];
            float step = (speed / (a - b).magnitude) * Time.fixedDeltaTime;
            float t = 0;
            while (t <= 1.0f) {
                if (!paused) {
                    t += step;
                    rb.MovePosition(Vector3.Lerp(a, b, t));
                }
                yield return new WaitForFixedUpdate();
            }
            currentIndex = (currentIndex + 1) % pattern.Count;
        }
    }
}

