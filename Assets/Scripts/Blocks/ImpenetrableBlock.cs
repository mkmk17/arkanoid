﻿using UnityEngine;

//Impenetrable block that cannot be destroyed.
public class ImpenetrableBlock : Block {

    override protected void Start() {
        BlockColor = Color.white;
        base.Start();
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Ball") {
            SoundPlayer.Instance.PlayBlockSound();
        }
    }
}
