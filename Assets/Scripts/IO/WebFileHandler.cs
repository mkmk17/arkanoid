﻿using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;

public class WebFileHandler : FileHandler {

    public WebFileHandler(string _path) : base(_path) {
        if (!new Regex(@"^(https?)|(file):\/\/").IsMatch(_path)) {
            path = @"file://" + _path;
        }
    }

    public override IEnumerator FileExists(string fileName, Action<bool> callback) {
        WWW w = new WWW(Path.Combine(path, fileName));
        yield return w;
        callback(string.IsNullOrEmpty(w.error));
    }

    public override IEnumerator GetFile(string fileName, Action<string> callback) {
        WWW w = new WWW(Path.Combine(path, fileName));
        yield return w;
        callback(w.text);
    }

    public override IEnumerator GetSprite(string fileName, Action<Sprite> callBack)
    {
        WWW w = new WWW(Path.Combine(path, fileName));
        yield return w;
        byte[] data = w.bytes;
        Texture2D texture = new Texture2D(64, 64, TextureFormat.ARGB32, false);
        texture.LoadImage(data);
        texture.name = Path.GetFileNameWithoutExtension(path);
        callBack(Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f)));
    }
}
