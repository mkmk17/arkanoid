﻿using System.Collections;
using System;
using UnityEngine;

// Abstract class for handling files. It currently has webGL and PC implementations.
// Since web is async everything has IEnumerator as result type even though PC is sync.
public abstract class FileHandler {

    protected string path;

    protected FileHandler(string _path) {
        path = _path;
    }

    public static FileHandler GetFileHandler(string _path) {
        #if UNITY_WEBGL
            return new WebFileHandler(_path);
        #else
            return new PCFileHandler(_path);
        #endif
    }

    public abstract IEnumerator FileExists(string fileName, Action<bool> callback);

    public abstract IEnumerator GetFile(string fileName, Action<string> callback);

    public abstract IEnumerator GetSprite(string fileName, Action<Sprite> callBack);
}
