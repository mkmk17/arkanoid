﻿using System.IO;
using System.Collections;
using System;
using UnityEngine;

public class PCFileHandler : FileHandler {
    public PCFileHandler(string _path) : base(_path) {
    }

    public override IEnumerator FileExists(string fileName, Action<bool> callback) {
        yield return null;
        callback(File.Exists(Path.Combine(path, fileName)));
    }

    public override IEnumerator GetFile(string fileName, Action<string> callback) {
        yield return null;
        callback(File.ReadAllText(Path.Combine(path, fileName)));
    }

    public override IEnumerator GetSprite(string fileName, Action<Sprite> callBack)
    {
        yield return null;
        byte[] data = File.ReadAllBytes(Path.Combine(path, fileName));
        Texture2D texture = new Texture2D(64, 64, TextureFormat.ARGB32, false);
        texture.LoadImage(data);
        texture.name = Path.GetFileNameWithoutExtension(path);
        callBack(Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f)));
    }
}
