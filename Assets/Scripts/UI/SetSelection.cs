﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;

//Class responsible for handling set selection screen.
public class SetSelection : MonoBehaviour
{
    public GameObject ButtonPrefab;

    void Start() {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        InitButtons();
        GetHighScores();
    }

    private void GetHighScores()
    {
        Dictionary<string, int> savedScores = SaveUtils.GetSetsScores();
        foreach (GameObject levelButton in GameObject.FindGameObjectsWithTag("Level Button"))
        {
            string setName = levelButton.GetComponentInChildren<Text>().text;
            if (savedScores.ContainsKey(setName) && savedScores[setName] != 0)
            {
                levelButton.GetComponent<Image>().color = Color.green;
                levelButton.transform.GetComponentsInChildren<Text>().First(text => text.gameObject.tag == "HighScore")
                    .text = savedScores[setName].ToString();
            }
            else
            {
                levelButton.transform.GetComponentsInChildren<Text>().First(text => text.gameObject.tag == "HighScore")
                    .text = "";
            }
        }
    }

    private void InitButtons()
    {
        List<string> setList = SetTracker.Instance.SetList;
        foreach (var setName in setList)
        {
            GameObject button = Instantiate(ButtonPrefab, transform);
            button.transform.Find("Text").GetComponent<Text>().text = setName;
            var name1 = setName;
            button.GetComponent<Button>().onClick.AddListener(() => { button.GetComponent<MenuButton>().LevelSelect(name1); });
        }
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            SceneManager.LoadScene("Main Menu");
        }
    }
}
