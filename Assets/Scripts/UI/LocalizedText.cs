﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LocalizedText : MonoBehaviour {

    Text textComponent;

    [SerializeField]
    public string val;

    [SerializeField]
    private List<string> parameters;

    public string Key {
        get {
            return textComponent.text;
        }
    }

	void Awake () {
        textComponent = GetComponent<Text>();
        ChangeText();
        parameters = new List<string>();
	}

    public void ChangeText() {
        textComponent.text = Translations.Instance.GetLocalizedValue(val, parameters);
    }

    public void SetParam(int index, string param) {
        while (index >= parameters.Count) {
            parameters.Add("");
        }
        parameters[index] = param;
        ChangeText();
    }

    public void SetVal(string newVal) {
        val = newVal;
        ChangeText();
    }

    public string GetVal() {
        return val;
    }

    public string Getparam(int index) {
        return parameters[index];
    }
}

