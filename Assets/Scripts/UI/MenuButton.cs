﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Class responsible for storing functions handling pressing buttons on menu.
public class MenuButton : MonoBehaviour {

    public void StartButtonPressed() {
        SceneManager.LoadScene("Set Select");
    }

    public void ExitButtonPressed() {
        Application.Quit();
    }

    public void LevelSelect(string setName) {
        Game.Instance.StartSet(setName);
    }

    public void BackToMainMenu() {
        SceneManager.LoadScene("Main Menu");
    }

    public void ClearSavePressed() {
        SaveUtils.ClearSave();
        GameObject.FindGameObjectWithTag("SaveClearedText").GetComponent<Text>().enabled = true;
    }

    public void InstructionsButtonPressed() {
        SceneManager.LoadScene("Instructions");
    }

    public void OptionsButtonPressed() {
        SceneManager.LoadScene("Options");
    }

    public void MusicButtonPressed() {
        int music = PlayerPrefs.GetInt("Music", 1);
        music = (music + 1) % 2;
        Options.SetButton(gameObject, music == 1 ? "yes" : "no");
        PlayerPrefs.SetInt("Music", music);
    }

    public void SoundButtonPressed() {
        int sound = PlayerPrefs.GetInt("Sound", 1);
        sound = (sound + 1) % 2;
        Options.SetButton(gameObject, sound == 1 ? "yes" : "no");
        PlayerPrefs.SetInt("Sound", sound);
    }

    public void ControlsButtonPressed() {
        int controls = PlayerPrefs.GetInt("Controls", 1);
        controls = (controls + 1) % 2;
        Options.SetButton(gameObject, controls == 1 ? "mouse" : "keyboard");
        PlayerPrefs.SetInt("Controls", controls);
    }

    public void LanguageButtonPressed() {
        string newLanguage = Options.NextLanguage(gameObject);
        PlayerPrefs.SetString("Language", newLanguage);
        foreach (LocalizedText text in FindObjectsOfType<LocalizedText>()) {
            text.ChangeText();
        }
    }
}
