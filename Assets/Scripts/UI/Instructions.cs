﻿using UnityEngine;
using UnityEngine.SceneManagement;

//Class responsible for handling instructions screen.
public class Instructions : MonoBehaviour {

    void Start() {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            SceneManager.LoadScene("Main Menu");
        }
    }
}
