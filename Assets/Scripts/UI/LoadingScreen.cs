﻿using UnityEngine;
using UnityEngine.SceneManagement;

//Loading screen. Just waiting for translations or sets to load.

public class LoadingScreen : MonoBehaviour
{

    public static bool InitialLoading = true;

	void Awake ()
	{
	    if (!InitialLoading) return;
	    
        /*
        FileLevelCreator.Instance.WriteSet("A", "A.png", 999);
        FileLevelCreator.Instance.WriteSet("B", "B.png", 1111);
        FileLevelCreator.Instance.WriteSet("C", "C.png", 1222);
        FileLevelCreator.Instance.WriteSet("D", "D.png", 1333);
        FileLevelCreator.Instance.WriteSet("E", "E.png", 1444);
        */
        
	    Translations.Instance.Init();
	    SetTracker.Instance.Init();
	}
	
	void Update () {
        if (InitialLoading) {
            if (!Translations.Instance.IsLoaded() || !SetTracker.Instance.Ready)
                return;
            InitialLoading = false;
            SceneManager.LoadScene("Main Menu");
        }
        else
        {
            if (!SetTracker.Instance.Ready) return;
            SceneManager.LoadScene("Gameplay");
        }
	}
}
