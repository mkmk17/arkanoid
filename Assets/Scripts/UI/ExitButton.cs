﻿using UnityEngine;

public class ExitButton : MonoBehaviour {

	void Start () {
#if UNITY_WEBGL
        gameObject.SetActive(false);
#else
        if (Application.isEditor) {
            gameObject.SetActive(false);
        }
#endif
    }
}
