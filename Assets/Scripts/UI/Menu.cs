﻿using UnityEngine;

//Class responsible for handling main menu screen.
public class Menu : MonoBehaviour {

    void Start() {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }
    }
}
