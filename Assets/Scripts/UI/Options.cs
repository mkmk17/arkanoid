﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;
using System.Linq;

//Class responsible for handling options screen
public class Options : MonoBehaviour {

    public GameObject MusicButton;
    public GameObject SoundButton;
    public GameObject ControlsButton;
    public GameObject LanguageButton;

    void Start() {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        int music = PlayerPrefs.GetInt("Music", 1);
        int sound = PlayerPrefs.GetInt("Sound", 1);
        int controls = PlayerPrefs.GetInt("Controls", 1);
        string language = PlayerPrefs.GetString("Language", Translations.DEFAULT_LANGUAGE);
        SetButton(MusicButton, music == 1 ? "yes" : "no");
        SetButton(SoundButton, sound == 1 ? "yes" : "no");
        SetButton(ControlsButton, controls == 1 ? "mouse" : "keyboard");
        SetLanguage(LanguageButton, language);
    }

    public static string NextLanguage(GameObject button) {
        Translations tm = Translations.Instance;
        tm.CurrentLangIndex++;
        tm.CurrentLangIndex %= tm.languages.Count;
        string newLanguage = tm.languages[tm.CurrentLangIndex];
        button.GetComponentInChildren<Text>().text = newLanguage;
        return newLanguage;
    }

    public static void SetButton(GameObject button, string condition) {
        button.GetComponent<Image>().color = 
            (condition == "yes" || condition == "mouse") ? Color.green :Color.red;
        button.GetComponentInChildren<LocalizedText>().SetVal(condition);
    }

    public static void SetLanguage(GameObject button, string language) {
        button.GetComponentInChildren<Text>().text = language;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            SceneManager.LoadScene("Main Menu");
        }
    }
}
